package com.bitberg.yw.congress_app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class Splash extends AppCompatActivity {
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sessionManager = new SessionManager(Splash.this);
        getSupportActionBar().hide();
        sessionManager.setIMGfilepath(null);
        sessionManager.setIMG(null);
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent in = new Intent(Splash.this, Login.class);
                    startActivity(in);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                }

            }


        };
        timer.start();


    }

    @Override
    public void onBackPressed() {
        //
    }
}
