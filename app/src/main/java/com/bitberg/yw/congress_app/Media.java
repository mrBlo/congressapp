package com.bitberg.yw.congress_app;

/**
 * Created by Yaw on 10/03/2016.
 */
public class Media {
    private String id, imageName, file_name, description, created, updated, deleted;

    public Media(String id, String imageName, String file_name, String description, String created, String updated, String deleted) {
        this.id = id;
        this.imageName = imageName;
        this.file_name = file_name;
        this.description = description;
        this.created = created;
        this.updated = updated;
        this.deleted = deleted;
    }

    public Media(String file_name, String imageName, String description) {
        this.file_name = file_name;
        this.imageName = imageName;

        this.description = description;
    }

    public Media(String id, String imageName, String file_name, String description) {
        this.id = id;
        this.imageName = imageName;
        this.file_name = file_name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return
                "email='" + file_name + "\n" +
                        "phone_no='" + description + "\n" +
                        "name='" + created + "\n" +
                        "topic='" + updated + "\n" +
                        "topic_description='" + imageName + "\n"
//                "biography='" + biography + "\n" +
//                "website='" + website
                ;
    }
}
