package com.bitberg.yw.congress_app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class EditProfile extends AppCompatActivity {
    ImageView log_Profile_pik;
    EditText log_firstname, log_lastname, log_email, log_mobile;
    String log_firstname_str, log_lastname_str, log_email_str, log_mobile_str;
    SessionManager sm;
    Context con = EditProfile.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        log_Profile_pik = (ImageView) findViewById(R.id.log_profile_pik);
        log_firstname = (EditText) findViewById(R.id.log_firstnameET);
        log_lastname = (EditText) findViewById(R.id.log_lastnameET);
        log_email = (EditText) findViewById(R.id.log_emailET);
        log_mobile = (EditText) findViewById(R.id.log_mobileET);
        sm = new SessionManager(con);

//        if (sm.get_is_logged_in()!=true){
//            Toast.makeText(con, "You are logged out", Toast.LENGTH_SHORT).show();
//            Intent in = new Intent(con, Login.class);
//            startActivity(in);
//            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//            finish();
//        }

        log_mobile.setText(sm.getLog_MobileNumber());
        log_email.setText(sm.getLog_Email());
        log_firstname.setText(sm.getLog_FirstName());
        log_lastname.setText(sm.getLog_LastName());


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Save Changes", Snackbar.LENGTH_LONG)
                        .setAction("Proceed", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {


                            }
                        }).show();
            }
        });
    }


    @Override
    public void onBackPressed() {
        Intent in = new Intent(getApplicationContext(), WelcomePage.class);
        //   in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }

}
