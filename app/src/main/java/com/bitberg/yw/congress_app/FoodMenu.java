package com.bitberg.yw.congress_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.ImageOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FoodMenu extends AppCompatActivity {

    ArrayList<MenuClass> allstds = new ArrayList<MenuClass>();
    GridView gv;
    SessionManager sm;
    Context con = FoodMenu.this;
    private static String url = "http://congressapi.bitberglimited.com/api/user/menu.json";
    MyAdapter stad;
    ProgressDialog progressDialog;
    AQuery aq;
    MenuClass foodmenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_menu);
        gv = (GridView) findViewById(R.id.list);

        sm = new SessionManager(con);

        aq = new AQuery(con);
        progressDialog = new ProgressDialog(con);

        //new MyAdapter(this, allstds);

        if (!isOnline(con)) {
            Toast.makeText(con, "No network connection", Toast.LENGTH_LONG).show();
        } else {
            sendImage(con);
        }
    }


    class MyAdapter extends BaseAdapter {
        private Context ctx;
        ArrayList<MenuClass> allstd;

        public MyAdapter(Context ctx, ArrayList<MenuClass> stds) {
            this.ctx = ctx;
            allstd = stds;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub

            return allstd.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stublist.get(i);
            return allstd.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }


        //NB: THIS FUNCTION WILL BE CALLED THE NUMBER OF TIMES RETURNED BY THE getcount METHOD
        @Override
        public View getView(int index, View convertView, ViewGroup parent) {
            //NB : THE FIRST ARGUMENT IS THE INDEX OF THE COLLECTION BEIGN PROCESSED
            //GETTING THE STUDENT OBJECT AT THAT INDEX
            MenuClass st = allstd.get(index);


            // TODO Auto-generated method stub
            //Requesting an inflater from system to dynamically inflate a layout with contents
            LayoutInflater linf = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //inflate fuction returns a view reference to the layout to be inflated
            View v = linf.inflate(R.layout.menu_list_item, null);
            if (index % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#f0f0f5"));

            } else {
                v.setBackgroundColor(Color.parseColor("#8e99d7"));
                //holder.age.setTextColor(Color.parseColor("#ffffff"));
            }

            //NB: V IS USED BECAUSE THE VIEWS ARE ON THE the v returned by the inflate method
            ImageView menu_pp = (ImageView) v.findViewById(R.id.menu_pp);
            TextView menu_date = (TextView) v.findViewById(R.id.menu_date);
            TextView menu_name = (TextView) v.findViewById(R.id.menu_name);
            TextView menu_type = (TextView) v.findViewById(R.id.menu_type);
            //now time to inflate the views on LAYOUT V
//            if (st.getMenu_imageName()==null){
//          //  menu_pp.setImageResource(st.getPhotoId());
//            menu_date.setText(st.getMenu_date());
//            menu_name.setText(st.getMenu_name());
//            menu_type.setText(st.getMenu_menu_type());
//              //  return v;
//            }
//else {
            //   use aquery here
            // menu_pp.setImageResource(st.getPhotoId());
            ImageOptions op = new ImageOptions();
            op.targetWidth = 0;
            op.fileCache = true;
            op.animation = AQuery.FADE_IN;
            op.round = 700;

            op.preset = null;
            op.fallback = R.drawable.menuuuu;
            op.memCache = true;
            AQuery aq = new AQuery(con);

            aq.id(menu_pp).image("http://congress.bitberglimited.com/uploads/food/" + st.getMenu_imageName(),
                    op);
            //   aq.id(menu_pp).image()
            menu_date.setText(st.getMenu_date().substring(0, 10));
            menu_name.setText(st.getMenu_name());
            menu_type.setText(st.getMenu_menu_type());


            return v;
        }


    }


    private void sendImage(final Context context) {

        System.out.println("------------------------------------------- " + url);

        /*THIS IS COMMENTED CODE BELOW IS USED TO GET OBJECTS FROM URL
         */
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        aq.progress(progressDialog).ajax(url, JSONArray.class,
                new AjaxCallback<JSONArray>() {
                    @Override
                    public void callback(String url, JSONArray json,
                                         AjaxStatus status) {

                        //AjaxStatus
                        try {
                            if (json != null) {
                                System.out.println("------------------------------------------- " + json + "\n" + status.getCode() + "\n" + status.getMessage());


                                if (status.getCode() == 200) {

                                    for (int i = 0; i < json.length(); i++) {
                                        JSONObject jObject = json.getJSONObject(i);
//                                        Toast.makeText(con, jObject.getString("name")
//                                                , Toast.LENGTH_SHORT).show();
                                        String id = jObject.getString("id");
                                        Log.d("id", id + "");

                                        String name = jObject.getString("name");
                                        Log.d("name", name + "");

                                        String date = jObject.getString("menu_date");
                                        Log.d("date", date + "");

                                        String menutype = jObject.getString("menu_type");
                                        Log.d("menutype", menutype + "");

                                        String imageName = jObject.getString("imageName");
                                        Log.d("imageName", imageName + "");


                                        foodmenu = new MenuClass(id, date, name, imageName, menutype);
                            /*
                            PLACING MENU INTO LIST
                                */
                                        allstds.add(foodmenu);
                                        //   Toast.makeText(con,allstds+"",Toast.LENGTH_LONG).show();

                                        MyAdapter stad = new MyAdapter(con, allstds);
                                        gv.setAdapter(stad);

                                    }

                                    //saving login cred to session manager
//                                    sm.setLog_UserID(json.getString("id"));
//                                    sm.setLog_LastName(json.getString("last_name"));
//                                    sm.setLog_FirstName(json.getString("first_name"));
//                                    sm.setLog_MobileNumber(json.getString("mobile_no"));
//                                    sm.setLog_Email(json.getString("email"));
//                                    sm.set_is_logged_in(Boolean.parseBoolean(json.getString("is_logged_in")));
//                                    Toast.makeText(Login.this, sm.get_is_logged_in()+"", Toast.LENGTH_SHORT).show();

                                } else {
                                    Toast.makeText(con, "Couldn't load menu"
                                            , Toast.LENGTH_LONG).show();
                                }

                            } else {
                                System.out.println("-----------" + json + "" + "--------------------------fail " + "\n" + status.getCode() + "\n" + status.getMessage());
                                Toast.makeText(con, status.getMessage(), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (final Exception ex) {
                            ex.printStackTrace();

                        }

                    }
                });
    }


    private boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }


    @Override
    public void onBackPressed() {
        Intent in = new Intent(getApplicationContext(), WelcomePage.class);
        //   in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }


}
