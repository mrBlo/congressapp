package com.bitberg.yw.congress_app;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Yaw on 8/02/2016.
 */
public class SessionManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "demo";

    public SessionManager(Context context) {
        super();
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setIMG(String imagepath) {
        //Storing name  in Pref
        editor.putString("IMAGEPATH", imagepath);
        editor.commit();
    }

    public String getIMG() {
        //Storing name  in Pref
        return pref.getString("IMAGEPATH", null);
    }

    public void setIMGfilepath(String IMGfilepath) {
        //Storing name  in Pref
        editor.putString("IMGfilepath", IMGfilepath);
        editor.commit();
    }

    public String getIMGfilepath() {
        //Storing name  in Pref
        return pref.getString("IMGfilepath", null);
    }


    public void setUsername(String userData) {
        //Storing name  in Pref
        editor.putString("Username", userData);
        editor.commit();
    }

    public String getUsername() {
        //Storing name  in Pref
        return pref.getString("Username", "username");
    }

    //////
    public void setSurname(String userData) {
        //Storing name  in Pref
        editor.putString("Surname", userData);
        editor.commit();
    }

    public String getSurname() {
        //Storing name  in Pref
        return pref.getString("Surname", null);
    }


    public void setFirstname(String userData) {
        //Storing name  in Pref
        editor.putString("firstname", userData);
        editor.commit();
    }

    public String getFirstname() {
        //Storing name  in Pref
        return pref.getString("firstname", null);
    }


    public void setothername(String userData) {
        //Storing name  in Pref
        editor.putString("othername", userData);
        editor.commit();
    }

    public String getothername() {
        //Storing name  in Pref
        return pref.getString("othername", "");
    }


    public void setphone(String userData) {
        //Storing name  in Pref
        editor.putString("phone", userData);
        editor.commit();
    }

    public String getphone() {
        //Storing name  in Pref
        return pref.getString("phone", null);
    }


    public void setemail(String userData) {
        //Storing name  in Pref
        editor.putString("email", userData);
        editor.commit();
    }

    public String getemail() {
        //Storing name  in Pref
        return pref.getString("email", null);
    }


    public void setschool(String userData) {
        //Storing name  in Pref
        editor.putString("school", userData);
        editor.commit();
    }

    public String getschool() {
        //Storing name  in Pref
        return pref.getString("school", null);
    }


    public void setyear(String userData) {
        //Storing name  in Pref
        editor.putString("year", userData);
        editor.commit();
    }

    public String getyear() {
        //Storing name  in Pref
        return pref.getString("year", null);
    }


    public void setpro(String userData) {
        //Storing name  in Pref
        editor.putString("prog", userData);
        editor.commit();
    }

    public String getprog() {
        //Storing name  in Pref
        return pref.getString("prog", null);
    }


    public void setgender(String userData) {
        //Storing name  in Pref
        editor.putString("gender", userData);
        editor.commit();
    }

    public String getgender() {
        //Storing name  in Pref
        return pref.getString("gender", null);
    }


    public void setfoodallergies(String userData) {
        //Storing name  in Pref
        editor.putString("foodallergies", userData);
        editor.commit();
    }

    public String getfoodallergies() {
        //Storing name  in Pref
        return pref.getString("foodallergies", null);
    }

    public void setchurchmember(String userData) {
        //Storing name  in Pref
        editor.putString("churchmember", userData);
        editor.commit();
    }

    public String getchurchmember() {
        //Storing name  in Pref
        return pref.getString("churchmember", null);
    }


    public void setlocalcongregation(String userData) {
        //Storing name  in Pref
        editor.putString("localcongregation", userData);
        editor.commit();
    }

    public String getlocalcongregation() {
        //Storing name  in Pref
        return pref.getString("localcongregation", null);
    }


    public void setlocation(String userData) {
        //Storing name  in Pref
        editor.putString("localcongregation", userData);
        editor.commit();
    }

    public String getlocation() {
        //Storing name  in Pref
        return pref.getString("location", null);
    }


    public void setpassword(String userData) {
        //Storing name  in Pref
        editor.putString("password", userData);
        editor.commit();
    }

    public String getpassword() {
        //Storing name  in Pref
        return pref.getString("password", null);
    }


    public void setpasswordalt(String userData) {
        //Storing name  in Pref
        editor.putString("passwordalt", userData);
        editor.commit();
    }

    public String getpasswordalt() {
        //Storing name  in Pref
        return pref.getString("passwordalt", null);
    }

    //Registered Users
    /*
    This stores the user cred of a logged in User
     */
    public void setLog_UserID(String userData) {
        //Storing name  in Pref
        editor.putString("Log_UserID", userData);
        editor.commit();
    }

    public String getLog_UserID() {
        //Storing name  in Pref
        return pref.getString("Log_UserID", "NULL");
    }

    public void setLog_LastName(String userData) {
        //Storing name  in Pref
        editor.putString("Log_LastName", userData);
        editor.commit();
    }

    public String getLog_LastName() {
        //Storing name  in Pref
        return pref.getString("Log_LastName", "NULL");
    }

    public void setLog_FirstName(String userData) {
        //Storing name  in Pref
        editor.putString("Log_FirstName", userData);
        editor.commit();
    }

    public String getLog_FirstName() {
        //Storing name  in Pref
        return pref.getString("Log_FirstName", "NULL");
    }

    public void setLog_MobileNumber(String userData) {
        //Storing name  in Pref
        editor.putString("Log_MobileNumber", userData);
        editor.commit();
    }

    public String getLog_MobileNumber() {
        //Storing name  in Pref
        return pref.getString("Log_MobileNumber", "NULL");
    }

    public void setLog_photo(String userData) {
        //Storing name  in Pref
        editor.putString("Log_photo", userData);
        editor.commit();
    }

    public String getLog_photo() {
        //Storing name  in Pref
        return pref.getString("Log_photo", "NULL");
    }


    public void setLog_Email(String userData) {
        //Storing name  in Pref
        editor.putString("Log_Email", userData);
        editor.commit();
    }

    public String getLog_Email() {
        //Storing name  in Pref
        return pref.getString("Log_Email", "NULL");
    }


    public void set_is_logged_in(Boolean userData) {
        //Storing name  in Pref
        editor.putBoolean("is_logged_in", userData);
        editor.commit();
    }

    public boolean get_is_logged_in() {
        //Storing name  in Pref
        return pref.getBoolean("is_logged_in", false);
    }

    /*
    Speaker Entries
     */
    public void set_speakerid(String userData) {
        //Storing name  in Pref
        editor.putString("speakerid", userData);
        editor.commit();
    }

    public String get_speakerid() {
        //Storing name  in Pref
        return pref.getString("speakerid", null);
    }

    public void set_speakerwebsite(String userData) {
        //Storing name  in Pref
        editor.putString("speakerwebsite", userData);
        editor.commit();
    }

    public String get_speakerwebsite() {
        //Storing name  in Pref
        return pref.getString("speakerwebsite", null);
    }


    public void set_speakerImage(String userData) {
        //Storing name  in Pref
        editor.putString("speakerImage", userData);
        editor.commit();
    }

    public String get_speakerImage() {
        //Storing name  in Pref
        return pref.getString("speakerImage", null);
    }

    public void set_speakerbio(String userData) {
        //Storing name  in Pref
        editor.putString("speakerbio", userData);
        editor.commit();
    }

    public String get_speakerbio() {
        //Storing name  in Pref
        return pref.getString("speakerbio", null);
    }

    public void set_speakerEmail(String userData) {
        //Storing name  in Pref
        editor.putString("speakeremail", userData);
        editor.commit();
    }

    public String get_speakerEmail() {
        //Storing name  in Pref
        return pref.getString("speakeremail", null);
    }

    public void set_speakerPhone(String userData) {
        //Storing name  in Pref
        editor.putString("speakerphone", userData);
        editor.commit();
    }

    public String get_speakerPhone() {
        //Storing name  in Pref
        return pref.getString("speakerphone", null);
    }

    public void set_speakerName(String userData) {
        //Storing name  in Pref
        editor.putString("speakername", userData);
        editor.commit();
    }

    public String get_speakerName() {
        //Storing name  in Pref
        return pref.getString("speakername", null);
    }

    public void set_speakerTopic(String userData) {
        //Storing name  in Pref
        editor.putString("speakertopic", userData);
        editor.commit();
    }

    public String get_speakerTopic() {
        //Storing name  in Pref
        return pref.getString("speakertopic", null);
    }


    public void set_speakerTopicDesc(String userData) {
        //Storing name  in Pref
        editor.putString("speakertopicdes", userData);
        editor.commit();
    }

    public String get_speakerTopicDesc() {
        //Storing name  in Pref
        return pref.getString("speakertopicdes", null);
    }

    public void set_partiid(String userData) {
        //Storing name  in Pref
        editor.putString("partiid", userData);
        editor.commit();
    }

    public String get_partiid() {
        //Storing name  in Pref
        return pref.getString("partiid", null);
    }

    public void set_partimage(String userData) {
        //Storing name  in Pref
        editor.putString("partimage", userData);
        editor.commit();
    }

    public String get_partimage() {
        //Storing name  in Pref
        return pref.getString("partimage", null);
    }

    public void set_partfname(String userData) {
        //Storing name  in Pref
        editor.putString("partfname", userData);
        editor.commit();
    }

    public String get_partfname() {
        //Storing name  in Pref
        return pref.getString("partfname", null);
    }

    public void set_partsurname(String userData) {
        //Storing name  in Pref
        editor.putString("partsurname", userData);
        editor.commit();
    }

    public String get_partsurname() {
        //Storing name  in Pref
        return pref.getString("partsurname", null);
    }

    public void set_partoname(String userData) {
        //Storing name  in Pref
        editor.putString("partoname", userData);
        editor.commit();
    }

    public String get_partoname() {
        //Storing name  in Pref
        return pref.getString("partoname", null);
    }

    public void set_partphone(String userData) {
        //Storing name  in Pref
        editor.putString("partphone", userData);
        editor.commit();
    }

    public String get_partphone() {
        //Storing name  in Pref
        return pref.getString("partphone", null);
    }

    public void set_parttype(String userData) {
        //Storing name  in Pref
        editor.putString("parttype", userData);
        editor.commit();
    }

    public String get_parttype() {
        //Storing name  in Pref
        return pref.getString("parttype", null);
    }

    public void set_partyear(String userData) {
        //Storing name  in Pref
        editor.putString("partyear", userData);
        editor.commit();
    }

    public String get_partyear() {
        //Storing name  in Pref
        return pref.getString("partyear", null);
    }

    public void set_partschool(String userData) {
        //Storing name  in Pref
        editor.putString("partschool", userData);
        editor.commit();
    }

    public String get_partschool() {
        //Storing name  in Pref
        return pref.getString("partschool", null);
    }

    /*
    Media
     */
    public void set_mediaid(String userData) {
        //Storing name  in Pref
        editor.putString("mediaid", userData);
        editor.commit();
    }

    public String get_mediaid() {
        //Storing name  in Pref
        return pref.getString("mediaid", null);
    }

    public void set_medianame(String userData) {
        //Storing name  in Pref
        editor.putString("medianame", userData);
        editor.commit();
    }

    public String get_medianame() {
        //Storing name  in Pref
        return pref.getString("medianame", null);
    }

    public void set_mediaimagename(String userData) {
        //Storing name  in Pref
        editor.putString("mediaimagename", userData);
        editor.commit();
    }

    public String get_mediaimagename() {
        //Storing name  in Pref
        return pref.getString("mediaimagename", null);
    }

    public void set_mediadesc(String userData) {
        //Storing name  in Pref
        editor.putString("mediadesc", userData);
        editor.commit();
    }

    public String get_mediadesc() {
        //Storing name  in Pref
        return pref.getString("mediadesc", null);
    }

    public void set_mediacreated(String userData) {
        //Storing name  in Pref
        editor.putString("mediacreated", userData);
        editor.commit();
    }

    public String get_mediacreated() {
        //Storing name  in Pref
        return pref.getString("mediacreated", null);
    }

    public void set_mediaupdated(String userData) {
        //Storing name  in Pref
        editor.putString("mediaupdated", userData);
        editor.commit();
    }

    public String get_mediaupdated() {
        //Storing name  in Pref
        return pref.getString("mediaupdated", null);
    }

    public void set_mediadeleted(String userData) {
        //Storing name  in Pref
        editor.putString("mediadeleted", userData);
        editor.commit();
    }

    public String get_mediadeleted() {
        //Storing name  in Pref
        return pref.getString("mediadeleted", null);
    }


    /*
    Questions
     */

    public void set_ques(String userData) {
        //Storing name  in Pref
        editor.putString("ques", userData);
        editor.commit();
    }

    public String get_ques() {
        //Storing name  in Pref
        return pref.getString("ques", null);
    }

    public void set_ques_created(String userData) {
        //Storing name  in Pref
        editor.putString("ques_created", userData);
        editor.commit();
    }

    public String get_ques_created() {
        //Storing name  in Pref
        return pref.getString("ques_created", null);
    }

    public void set_ques_firstname(String userData) {
        //Storing name  in Pref
        editor.putString("ques_firstname", userData);
        editor.commit();
    }

    public String get_ques_firstname() {
        //Storing name  in Pref
        return pref.getString("ques_firstname", null);
    }

    public void set_ques_surname(String userData) {
        //Storing name  in Pref
        editor.putString("ques_surname", userData);
        editor.commit();
    }

    public String get_ques_surname() {
        //Storing name  in Pref
        return pref.getString("ques_surname", null);
    }

    public void set_ques_userphoto(String userData) {
        //Storing name  in Pref
        editor.putString("ques_userphoto", userData);
        editor.commit();
    }

    public String get_ques_userphoto() {
        //Storing name  in Pref
        return pref.getString("ques_userphoto", null);
    }

    public void set_ques_topic(String userData) {
        //Storing name  in Pref
        editor.putString("ques_topic", userData);
        editor.commit();
    }

    public String get_ques_topic() {
        //Storing name  in Pref
        return pref.getString("ques_topic", null);
    }

    public void set_ques_response(String userData) {
        //Storing name  in Pref
        editor.putString("ques_response", userData);
        editor.commit();
    }

    public String get_ques_response() {
        //Storing name  in Pref
        return pref.getString("ques_response", null);
    }

    public void set_ques_response_date(String userData) {
        //Storing name  in Pref
        editor.putString("ques_response_date", userData);
        editor.commit();
    }

    public String get_ques_response_date() {
        //Storing name  in Pref
        return pref.getString("ques_response_date", null);
    }


}
