package com.bitberg.yw.congress_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.ImageOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ParticipantsActivity extends AppCompatActivity {
    ArrayList<User> allstd = new ArrayList<User>();
    GridView gv;
    SessionManager sm;
    Context con = ParticipantsActivity.this;
    private static String url = "http://congressapi.bitberglimited.com/api/user/users.json";
    MyAdapter stad;
    ProgressDialog progressDialog;
    AQuery aq;
    User user;
    ArrayList<User> originalList;

    User st;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_participants);
        gv = (GridView) findViewById(R.id.list);
        sm = new SessionManager(con);
        aq = new AQuery(con);
        progressDialog = new ProgressDialog(con);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        EditText myFilter = (EditText) findViewById(R.id.myFilter);


        if (!isOnline(con)) {
            Toast.makeText(con, "No network connection", Toast.LENGTH_LONG).show();
        } else {
            sendImage(con);
        }


        myFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                stad.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        gv.setTextFilterEnabled(true);


        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub

                User selectedBus = (User) parent
                        .getItemAtPosition(position);
                Toast.makeText(
                        con,
                        "You selected " + selectedBus.getSurname() + " " + selectedBus.getFirstname(),
                        Toast.LENGTH_LONG).show();
                Intent i = new Intent(con, ParticipantProfile.class);

                SessionManager speakSession = new SessionManager(con);
                speakSession.set_partiid(selectedBus.getId());
                speakSession.set_partimage(selectedBus.getImage());
                speakSession.set_partfname(selectedBus.getFirstname());
                speakSession.set_partsurname(selectedBus.getSurname());
                speakSession.set_partoname(selectedBus.getOthername());
                speakSession.set_partphone(selectedBus.getPhone());
                speakSession.set_partschool(selectedBus.getSchool());
                speakSession.set_parttype(selectedBus.getParticipant_type());
                speakSession.set_partyear(selectedBus.getYear());


                startActivity(i);
                overridePendingTransition(R.anim.fade_in
                        , R.anim.fade_out);
                finish();

            }
        });

    }


    class MyAdapter extends ArrayAdapter<User> {
        private Context ctx;
        // ArrayList<User> allstd;

        //
        // ArrayList<User> countryList;
        ContactFilter filter;


        public MyAdapter(Context context, int textViewResourceId, ArrayList<User> allstd) {
            // TODO Auto-generated constructor stub
            super(context, textViewResourceId, allstd);
            // this.ctx = ctx;
            //this.ctrys = ctrys;


            this.ctx = context;
            //this.
            allstd = new ArrayList<User>();
            // this.
            allstd.addAll(allstd);

            //this.
            originalList = new ArrayList<User>();
            //this.
            originalList.addAll(allstd);

        }

//        public MyAdapter(Context ctx, ArrayList<User> stds) {
//            this.ctx = ctx;
//            allstd = stds;
//        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub

            return allstd.size();
        }


        @Override
        public Filter getFilter() {
            if (filter == null) {
                filter = new ContactFilter();
            }
            return filter;
        }


        @Override
        public User getItem(int position) {
            // TODO Auto-generated method stublist.get(i);
            return allstd.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }


        //NB: THIS FUNCTION WILL BE CALLED THE NUMBER OF TIMES RETURNED BY THE getcount METHOD
        @Override
        public View getView(int index, View convertView, ViewGroup parent) {
            //NB : THE FIRST ARGUMENT IS THE INDEX OF THE COLLECTION BEIGN PROCESSED
            //GETTING THE STUDENT OBJECT AT THAT INDEX
            st = allstd.get(index);


            // TODO Auto-generated method stub
            //Requesting an inflater from system to dynamically inflate a layout with contents
            LayoutInflater linf = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //inflate fuction returns a view reference to the layout to be inflated
            View v = linf.inflate(R.layout.speaker_list_item, null);
            if (index % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#f0f0f5"));

            } else {
                v.setBackgroundColor(Color.parseColor("#8e99d7"));
                //holder.age.setTextColor(Color.parseColor("#ffffff"));
            }

            //NB: V IS USED BECAUSE THE VIEWS ARE ON THE the v returned by the inflate method
            ImageView menu_pp = (ImageView) v.findViewById(R.id.speaker_pp);
            TextView menu_topic = (TextView) v.findViewById(R.id.speaker_topic);//participant type
            TextView menu_name = (TextView) v.findViewById(R.id.speaker_name);
            TextView menu_phone = (TextView) v.findViewById(R.id.speaker_phone);
            //now time to inflate the views on LAYOUT V
            //   use aquery here
            // menu_pp.setImageResource(st.getPhotoId());
            ImageOptions op = new ImageOptions();
            op.targetWidth = 0;
            op.fileCache = true;
            op.animation = AQuery.FADE_IN;
            op.round = 700;

            op.preset = null;
            op.fallback = R.drawable.user;
            op.memCache = true;
            AQuery aq = new AQuery(con);
            aq.id(menu_pp).image("http://congress.bitberglimited.com/uploads/user/" + st.getImage(),
                    op);
            //  aq.im
            menu_topic.setText(st.getParticipant_type());
            //Toast.makeText(con,st.getOthername(),Toast.LENGTH_SHORT).show();
            menu_name.setText(st.getFirstname() + " " + st.getSurname());
            menu_phone.setText(st.getSchool());

            return v;
        }
    }

    private void sendImage(final Context context) {

        System.out.println("------------------------------------------- " + url);

        /*THIS IS COMMENTED CODE BELOW IS USED TO GET OBJECTS FROM URL
         */
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        aq.progress(progressDialog).ajax(url, JSONArray.class,
                new AjaxCallback<JSONArray>() {
                    @Override
                    public void callback(String url, JSONArray json,
                                         AjaxStatus status) {

                        //AjaxStatus
                        try {
                            if (json != null) {
                                System.out.println("------------------------------------------- " + json + "\n" + status.getCode() + "\n" + status.getMessage());

                                if (status.getCode() == 200) {

                                    for (int i = 0; i < json.length(); i++) {
                                        JSONObject jObject = json.getJSONObject(i);

                                        String id = jObject.getString("id");
                                        Log.d("id", id + "");
                                        String first_name = jObject.getString("first_name");
                                        Log.d("first_name", first_name + "");
                                        String last_name = jObject.getString("last_name");
                                        Log.d("last_name", last_name + "");
                                        String other_name = jObject.getString("other_name");
                                        Log.d("other_name", other_name + "");

                                        String mobile_no = jObject.getString("mobile_no");
                                        Log.d("mobile_no", mobile_no + "");
                                        String user_photo = jObject.getString("user_photo");
                                        Log.d("user_photo", user_photo + "");
                                        String participant_type = jObject.getString("participant_type");
                                        Log.d("participant_type", participant_type + "");
                                        String level = jObject.getString("level");
                                        Log.d("level", level + "");
                                        String school = jObject.getString("school");
                                        Log.d("school", school + "");

                                        user = new User(user_photo, id, last_name, first_name, other_name, mobile_no, school, level, participant_type);

                            /*
                            PLACING MENU INTO LIST
                                */
                                        allstd.add(user);
                                        // Toast.makeText(con,allstds+"",Toast.LENGTH_LONG).show();

                                        stad = new MyAdapter(con, R.layout.speaker_list_item, allstd);
                                        gv.setAdapter(stad);

                                    }

                                } else {
                                    Toast.makeText(con, "Couldn't load menu"
                                            , Toast.LENGTH_LONG).show();
                                }

                            } else {
                                System.out.println("-----------" + json + "" + "--------------------------fail " + "\n" + status.getCode() + "\n" + status.getMessage());
                                Toast.makeText(con, status.getMessage(), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (final Exception ex) {
                            ex.printStackTrace();

                        }

                    }
                });

    }

    private boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }


    private class ContactFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            constraint = constraint.toString().toLowerCase();
            Filter.FilterResults results = new FilterResults();
            if (constraint != null && constraint.toString().length() > 0) {
                ArrayList<User> filteredItems = new ArrayList<User>();
                for (int i = 0; i < originalList.size(); i++) {
                    User country = originalList.get(i);
                    if (country.toString().toLowerCase().contains(constraint))
                        filteredItems.add(country);
                }
                results.count = filteredItems.size();
                results.values = filteredItems;
            } else {
                synchronized (this) {
                    results.values = originalList;
                    results.count = originalList.size();
                }
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // allstd = (ArrayList<User>) results.values;
//            notifyDataSetChanged();
//              clear();
//            for (int i=0;i<allstd.size();i++){
//               add(allstd.get(i));
//               notifyDataSetInvalidated();
//            }

            if (results.count == 0)
                stad.notifyDataSetInvalidated();
            else {
                allstd = (ArrayList<User>) results.values;
                stad.notifyDataSetChanged();
            }


        }
    }


    @Override
    public void onBackPressed() {
        Intent in = new Intent(getApplicationContext(), WelcomePage.class);
        //   in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }


}
