package com.bitberg.yw.congress_app;

/**
 * Created by Yaw on 9/03/2016.
 */
public class MenuClass {
    private String menuid, menutype_id, menu_date, menu_name,
            menu_created_at, menu_updated_at, menu_deleted_at,
            menu_imageName, menu_menu_type;

    public MenuClass(String menuid,
                     String menu_date, String menu_name,
                     String menu_imageName, String menu_menu_type) {
        this.menuid = menuid;

        this.menu_date = menu_date;
        this.menu_name = menu_name;
        this.menu_imageName = menu_imageName;
        this.menu_menu_type = menu_menu_type;
    }


    public MenuClass(String menuid, String menutype_id, String menu_date, String menu_name,
                     String menu_created_at, String menu_updated_at, String menu_deleted_at,
                     String menu_imageName, String menu_menu_type) {
        this.menuid = menuid;
        this.menutype_id = menutype_id;
        this.menu_date = menu_date;
        this.menu_name = menu_name;
        this.menu_created_at = menu_created_at;
        this.menu_updated_at = menu_updated_at;
        this.menu_deleted_at = menu_deleted_at;
        this.menu_imageName = menu_imageName;
        this.menu_menu_type = menu_menu_type;
    }


    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    public String getMenutype_id() {
        return menutype_id;
    }

    public void setMenutype_id(String menutype_id) {
        this.menutype_id = menutype_id;
    }

    public String getMenu_date() {
        return menu_date;
    }

    public void setMenu_date(String menu_date) {
        this.menu_date = menu_date;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public String getMenu_created_at() {
        return menu_created_at;
    }

    public void setMenu_created_at(String menu_created_at) {
        this.menu_created_at = menu_created_at;
    }

    public String getMenu_updated_at() {
        return menu_updated_at;
    }

    public void setMenu_updated_at(String menu_updated_at) {
        this.menu_updated_at = menu_updated_at;
    }

    public String getMenu_deleted_at() {
        return menu_deleted_at;
    }

    public void setMenu_deleted_at(String menu_deleted_at) {
        this.menu_deleted_at = menu_deleted_at;
    }

    public String getMenu_imageName() {
        return menu_imageName;
    }

    public void setMenu_imageName(String menu_imageName) {
        this.menu_imageName = menu_imageName;
    }

    public String getMenu_menu_type() {
        return menu_menu_type;
    }

    public void setMenu_menu_type(String menu_menu_type) {
        this.menu_menu_type = menu_menu_type;
    }


    @Override
    public String toString() {
        return menu_date + "\n" +
                menu_name + "\n" +
                menu_imageName + "\n" +
                menu_menu_type
                ;
    }
}



