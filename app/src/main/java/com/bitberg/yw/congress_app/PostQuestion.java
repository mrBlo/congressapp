package com.bitberg.yw.congress_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.ImageOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PostQuestion extends AppCompatActivity {
    ImageView postpp;
    TextView postname;
    EditText postques;
    SessionManager sm;
    Context con = PostQuestion.this;
    private static String url = "http://congressapi.bitberglimited.com/api/user/question.json";
    ProgressDialog progressDialog;
    AQuery aq;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_question);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        postpp = (ImageView) findViewById(R.id.postpp);
        postname = (TextView) findViewById(R.id.postname);
        postques = (EditText) findViewById(R.id.postques);
        sm = new SessionManager(con);
        aq = new AQuery(con);
        progressDialog = new ProgressDialog(con);

        if (!sm.getLog_photo().equalsIgnoreCase("null")) {
            ImageOptions op = new ImageOptions();
            op.targetWidth = 0;
            op.fileCache = true;
            op.animation = AQuery.FADE_IN;
            op.round = 700;

            op.preset = null;
            op.fallback = R.drawable.user;
            op.memCache = true;
            AQuery aq = new AQuery(con);
            PostQuestion.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


            //   menu_pp.setImageResource(R.drawable.menuuuu);
            aq.id(postpp).image("http://congress.bitberglimited.com/uploads/user/" + sm.getLog_photo(),
                    op);
            Toast.makeText(con, sm.getLog_photo(), Toast.LENGTH_SHORT).show();

        }


        if (!sm.getLog_LastName().equalsIgnoreCase("null")) {
            postname.setText(sm.getLog_LastName() + " " + sm.getLog_FirstName());
        }


        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Post Question", Snackbar.LENGTH_LONG)
                        .setAction("Proceed", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                InputMethodManager imm = (InputMethodManager) PostQuestion.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(fab.getWindowToken(), 0);
                                validate();
                            }
                        }).show();
            }
        });

    }


    public void validate() {
        boolean valid = true;

        String question = postques.getText().toString();

        if (question.isEmpty()) {
            postques.setError("Empty text field");

        } else if (question.length() > 160) {
            postques.setError("Maximum of 160 characters");

        } else {
            postques.setError(null);
            // _passwordText.setError(null);
            if (!isOnline(PostQuestion.this)) {
                Toast.makeText(PostQuestion.this, "No network connection", Toast.LENGTH_LONG).show();
            }

            ////
            else {

                sendImage(PostQuestion.this, question);
            }
        }
    }


    private boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }


    private void sendImage(final Context context, final String question_
    ) {


        Map<String, Object> params = new HashMap<String, Object>();
        params.put("question", question_);
        params.put("user ", sm.getLog_UserID());
        params.put("topic", null);


        System.out.println("------------------------------------------- " + params);

        System.out.println("------------------------------------------- " + url);

        /*THIS IS COMMENTED CODE BELOW IS USED TO GET OBJECTS FROM URL
         */
        progressDialog.setMessage("Posting...");
        progressDialog.setCancelable(false);


        aq.progress(progressDialog).ajax(url, params, JSONObject.class,
                new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject json,
                                         AjaxStatus status) {

                        //AjaxStatus
                        try {
                            if (json != null) {
                                System.out.println("------------------------------------------- " + json + "\n" + status.getCode() + "\n" + status.getMessage());
                                //  int success=json.getInt(TAG_SUCCESS);
                                //  Log.e("++++++++++ ",  json.getString("message"));

//                                Toast.makeText(Login.this, "*******" +status.getCode()+"\n" + status.getMessage()
//                                        + "\n" + json
//                                        //+ "\n" + json.getJSONObject(0).getString("result")
//                                        ,Toast.LENGTH_LONG).show();
//                                sm.setUsername(username_);
//                                sm.setpassword(password_);
                                if (status.getCode() == 200) {
                                    Toast.makeText(PostQuestion.this, "Post Complete"
                                            , Toast.LENGTH_SHORT).show();
//                                    sm.setUsername(username_);
//                                    sm.setpassword(password_);

                                    //saving login cred to session manager
//                                    sm.setLog_UserID(json.getString("id"));
//                                    sm.setLog_LastName(json.getString("last_name"));
//                                    sm.setLog_FirstName(json.getString("first_name"));
//                                    sm.setLog_MobileNumber(json.getString("mobile_no"));
//                                    sm.setLog_Email(json.getString("email"));
//                                    sm.set_is_logged_in(Boolean.parseBoolean(json.getString("is_logged_in")));

                                    Intent in = new Intent(con, QuestionActivity.class);
                                    startActivity(in);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    finish();
                                } else {
                                    Toast.makeText(PostQuestion.this, "Post Unsuccessful"
                                            , Toast.LENGTH_LONG).show();
                                }

                            } else {
                                System.out.println("-----------" + json + "" + "--------------------------fail " + "\n" + status.getCode() + "\n" + status.getMessage());
                                Toast.makeText(PostQuestion.this, "Post Failed", Toast.LENGTH_LONG).show();
                            }

                        } catch (final Exception ex) {
                            ex.printStackTrace();

                        }

                    }
                });


    }


    @Override
    public void onBackPressed() {
        Intent in = new Intent(getApplicationContext(), QuestionActivity.class);
        //   in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }

}
