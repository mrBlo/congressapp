package com.bitberg.yw.congress_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.ImageOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MediaActivity extends AppCompatActivity {
    ArrayList<Media> allstds = new ArrayList<Media>();
    GridView gv;
    SessionManager sm;
    Context con = MediaActivity.this;
    private static String url = "http://congressapi.bitberglimited.com/api/user/gallery.json";
    MyAdapter stad;
    ProgressDialog progressDialog;
    AQuery aq;
    Media media;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);
        gv = (GridView) findViewById(R.id.list);

        sm = new SessionManager(con);

        aq = new AQuery(con);
        progressDialog = new ProgressDialog(con);

        //new MyAdapter(this, allstds);

        if (!isOnline(con)) {
            Toast.makeText(con, "No network connection", Toast.LENGTH_LONG).show();
        } else {
            sendImage(con);
        }

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub

                Media selectedBus = (Media) parent
                        .getItemAtPosition(position);
//                Toast.makeText(
//                        con,
//                        "you selected " + selectedBus.getName(),
//                        Toast.LENGTH_LONG).show();
                Intent i = new Intent(con, MediaProfile.class);

                SessionManager speakSession = new SessionManager(con);
                speakSession.set_mediaid(selectedBus.getId());
                speakSession.set_mediaimagename(selectedBus.getImageName());
                speakSession.set_mediacreated(selectedBus.getCreated());
                speakSession.set_mediadeleted(selectedBus.getDeleted());
                speakSession.set_mediadesc(selectedBus.getDescription());
                speakSession.set_medianame(selectedBus.getFile_name());
                speakSession.set_mediaupdated(selectedBus.getUpdated());
//                speakSession.set_speakerTopic(selectedBus.getTopic());
//                speakSession.set_speakerTopicDesc(selectedBus.getTopic_description());
                startActivity(i);
                overridePendingTransition(R.anim.fade_in
                        , R.anim.fade_out);
                finish();

            }
        });


    }

    class MyAdapter extends BaseAdapter {
        private Context ctx;
        ArrayList<Media> allstd;

        public MyAdapter(Context ctx, ArrayList<Media> stds) {
            this.ctx = ctx;
            allstd = stds;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub

            return allstd.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stublist.get(i);
            return allstd.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }


        //NB: THIS FUNCTION WILL BE CALLED THE NUMBER OF TIMES RETURNED BY THE getcount METHOD
        @Override
        public View getView(int index, View convertView, ViewGroup parent) {
            //NB : THE FIRST ARGUMENT IS THE INDEX OF THE COLLECTION BEIGN PROCESSED
            //GETTING THE STUDENT OBJECT AT THAT INDEX
            Media st = allstd.get(index);


            // TODO Auto-generated method stub
            //Requesting an inflater from system to dynamically inflate a layout with contents
            LayoutInflater linf = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //inflate fuction returns a view reference to the layout to be inflated
            View v = linf.inflate(R.layout.media_list_item, null);
//            if (index % 2 == 0) {
//                v.setBackgroundColor(Color.parseColor("#f0f0f5"));
//
//            } else {
//                v.setBackgroundColor(Color.parseColor("#8e99d7"));
//                //holder.age.setTextColor(Color.parseColor("#ffffff"));
//            }

            //NB: V IS USED BECAUSE THE VIEWS ARE ON THE the v returned by the inflate method
            ImageView media_pp = (ImageView) v.findViewById(R.id.media_pp);
            TextView media_name = (TextView) v.findViewById(R.id.media_name);
            TextView media_imagename = (TextView) v.findViewById(R.id.media_imagename);
            TextView media_desc = (TextView) v.findViewById(R.id.media_desc);
            //now time to inflate the views on LAYOUT V
            //   use aquery here
            // menu_pp.setImageResource(st.getPhotoId());
            ImageOptions op = new ImageOptions();
            op.targetWidth = 0;
            op.fileCache = true;
            op.animation = AQuery.FADE_IN;
            op.round = 700;

            op.preset = null;
            op.fallback = R.drawable.gallery;
            op.memCache = true;
            AQuery aq = new AQuery(con);

//                aq.id(media_pp).image("http://congress.bitberglimited.com/uploads/media/" + st.getImageName(),
//                        op);
            //aq.im
            media_name.setText("Name: " + st.getFile_name());
            media_imagename.setText("File: " + st.getImageName()
                    //.substring(0,)+"..."
            );
            media_desc.setText("Desc: " + st.getDescription()
                    //   .substring(0,8)+"..."
            );

            return v;
        }

    }


    private void sendImage(final Context context) {

        System.out.println("------------------------------------------- " + url);

        /*THIS IS COMMENTED CODE BELOW IS USED TO GET OBJECTS FROM URL
         */
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        aq.progress(progressDialog).ajax(url, JSONArray.class,
                new AjaxCallback<JSONArray>() {
                    @Override
                    public void callback(String url, JSONArray json,
                                         AjaxStatus status) {

                        //AjaxStatus
                        try {
                            if (json != null) {
                                System.out.println("-------------- " + json + "\n" + status.getCode() + "\n" + status.getMessage());

//                                Toast.makeText(Login.this, "*******" +status.getCode()+"\n" + status.getMessage()
//                                        + "\n" + json
//                                        //+ "\n" + json.getJSONObject(0).getString("result")
//                                        ,Toast.LENGTH_LONG).show();

                                if (status.getCode() == 200) {

                                    for (int i = 0; i < json.length(); i++) {
                                        JSONObject jObject = json.getJSONObject(i);
//                                        Toast.makeText(con, jObject.getString("name")
//                                                , Toast.LENGTH_SHORT).show();
                                        String id = jObject.getString("id");
                                        Log.d("id", id + "");
                                        String file_name = jObject.getString("file_name");
                                        Log.d("file_name", file_name + "");
                                        String description = jObject.getString("description");
                                        Log.d("description", description + "");
                                        String imageName = jObject.getString("imageName");
                                        Log.d("imageName", imageName + "");
                                        String created_at = jObject.getString("created_at");
                                        Log.d("created_at", created_at + "");
                                        String updated_at = jObject.getString("updated_at");
                                        Log.d("updated_at", updated_at + "");
                                        String deleted_at = jObject.getString("deleted_at");
                                        Log.d("deleted_at", deleted_at + "");
//                                        String email = jObject.getString("email");
//                                        Log.d("email", email + "");
//                                        String topic_description = jObject.getString("topic_description");
//                                        Log.d("topic_description", topic_description + "");


                                        media = new Media(id, imageName, file_name, description, created_at, updated_at, deleted_at);
                            /*
                            PLACING MENU INTO LIST
                                */
                                        allstds.add(media);
                                        // Toast.makeText(con,allstds+"",Toast.LENGTH_LONG).show();

                                        MyAdapter stad = new MyAdapter(con, allstds);
                                        gv.setAdapter(stad);

                                    }

                                } else {
                                    Toast.makeText(con, "Couldn't load media items"
                                            , Toast.LENGTH_LONG).show();
                                }

                            } else {
                                System.out.println("-----------" + json + "" + "--------------------------fail " + "\n" + status.getCode() + "\n" + status.getMessage());
                                Toast.makeText(con, status.getMessage(), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (final Exception ex) {
                            ex.printStackTrace();

                        }

                    }
                });

    }


    private boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }


    @Override
    public void onBackPressed() {
        Intent in = new Intent(getApplicationContext(), WelcomePage.class);
        //   in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }

}