package com.bitberg.yw.congress_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {
    AlertDialog.Builder builder;
    EditText _usernameText, _passwordText;
    Button _loginButton;
    TextView _signupLink;
    SessionManager sm;
    Context mcontext;
    // private static String url = "http://congress.bitberglimited.com/login/api";
    //  private static String url = "http://tryourweb.altervista.org/LocalOfferte0.0/src/category_controller.php";
    // private static String url = "http://front.geevapp.com/api/user/login.json";
    private static String url = "http://congressapi.bitberglimited.com/api/user/login.json";
    //  private static String url =  "http://congressapi.bitberglimited.com/api/user/users.json";

    //   private static final String TAG_SUCCESS = "success";
    ProgressDialog progressDialog;
    AQuery aq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        _usernameText = (EditText) findViewById(R.id.input_username);
        _passwordText = (EditText) findViewById(R.id.input_password);
        _loginButton = (Button) findViewById(R.id.btn_login);
        _signupLink = (TextView) findViewById(R.id.link_signup);
        sm = new SessionManager(Login.this);
        aq = new AQuery(Login.this);
        progressDialog = new ProgressDialog(Login.this);

//        if (sm.get_is_logged_in()=false){
//            Toast.makeText(Login.this,"You are already logged in",Toast.LENGTH_SHORT).show();
//            Intent in = new Intent(Login.this, WelcomePage.class);
//            startActivity(in);
//            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//            finish();
//        }


        if (!sm.getUsername().equalsIgnoreCase("username")) {
            _usernameText.setText(sm.getUsername());
        }

//        if (!sm.getpassword().equalsIgnoreCase("password")) {
//            _passwordText.setText(sm.getpassword());
//        }

        //hiding keyboard
        Login.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//
//        InputMethodManager inputMethodManager= (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(null, 0);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) Login.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(_loginButton.getWindowToken(), 0);
                validate();
            }
        });

        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent in = new Intent(Login.this, RegistrationType.class);
                startActivity(in);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }


        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "..to Registration Page", Snackbar.LENGTH_LONG)
                        .setAction("Proceed", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent in = new Intent(Login.this, RegistrationType.class);
                                startActivity(in);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                finish();
                            }
                        }).show();
            }
        });
    }


    public void validate() {
        boolean valid = true;

        String username = _usernameText.getText().toString();
        String password = _passwordText.getText().toString();

        if (username.isEmpty()) {
            /*|| username.length()!=10*/
            _usernameText.setError("Enter a valid username");

        } else if (password.isEmpty() || password.length() < 5 || password.length() > 20) {
            _passwordText.setError("between 6 and 20 alphanumeric characters");

        } else {
            _usernameText.setError(null);
            _passwordText.setError(null);
            if (!isOnline(Login.this)) {
                Toast.makeText(Login.this, "No network connection", Toast.LENGTH_LONG).show();
            }

            ////
            else {

                sendImage(Login.this, username, password);

            }


        }

        //return valid;
    }


    private boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }


    private void sendImage(final Context context, final String username_,
                           final String password_) {


        Map<String, Object> params = new HashMap<String, Object>();
        params.put("phone", username_);
//type
        params.put("password", password_);

        System.out.println("------------------------------------------- " + params);

        System.out.println("------------------------------------------- " + url);

        /*THIS IS COMMENTED CODE BELOW IS USED TO GET OBJECTS FROM URL
         */
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);


        aq.progress(progressDialog).ajax(url, params, JSONObject.class,
                new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject json,
                                         AjaxStatus status) {

                        //AjaxStatus
                        try {
                            if (json != null) {
                                System.out.println("------------------------------------------- " + json + "\n" + status.getCode() + "\n" + status.getMessage());
                                //  int success=json.getInt(TAG_SUCCESS);
                                //  Log.e("++++++++++ ",  json.getString("message"));

//                                Toast.makeText(Login.this, "*******" +status.getCode()+"\n" + status.getMessage()
//                                        + "\n" + json
//                                        //+ "\n" + json.getJSONObject(0).getString("result")
//                                        ,Toast.LENGTH_LONG).show();
                                sm.setUsername(username_);
                                sm.setpassword(password_);
                                if (status.getCode() == 200) {
                                    Toast.makeText(Login.this, "Welcome " + json.getString("first_name")
                                            , Toast.LENGTH_SHORT).show();
                                    sm.setUsername(username_);
                                    sm.setpassword(password_);

                                    //saving login cred to session manager
                                    sm.setLog_UserID(json.getString("id"));
                                    sm.setLog_LastName(json.getString("last_name"));
                                    sm.setLog_FirstName(json.getString("first_name"));
                                    sm.setLog_MobileNumber(json.getString("mobile_no"));
                                    sm.setLog_Email(json.getString("email"));
                                    sm.set_is_logged_in(Boolean.parseBoolean(json.getString("is_logged_in")));

                                    Intent in = new Intent(Login.this, WelcomePage.class);
                                    startActivity(in);
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    finish();
                                } else {
                                    Toast.makeText(Login.this, "Login Error"
                                            , Toast.LENGTH_LONG).show();
                                }

                            } else {
                                System.out.println("-----------" + json + "" + "--------------------------fail " + "\n" + status.getCode() + "\n" + status.getMessage());
                                Toast.makeText(Login.this, status.getMessage(), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (final Exception ex) {
                            ex.printStackTrace();

                        }

                    }
                });


    }


    @Override
    public void onBackPressed() {

        builder = new AlertDialog.Builder(Login.this);
        builder.setMessage("Are you sure you want to exit?");
        builder.setTitle("ALERT");
        builder.setCancelable(false);
        builder.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                //	stopService(svc);
                Login.this.finish();
                System.exit(0);
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();


    }
}
