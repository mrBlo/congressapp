package com.bitberg.yw.congress_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.ImageOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class QuestionActivity extends AppCompatActivity {
    ArrayList<Question> allstds = new ArrayList<Question>();
    GridView gv;
    SessionManager sm;
    Context con = QuestionActivity.this;
    private static String url = "http://congressapi.bitberglimited.com/api/user/questions.json";
    MyAdapter stad;
    ProgressDialog progressDialog;
    AQuery aq;
    Question question;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Post Question", Snackbar.LENGTH_LONG)
                        .setAction("Proceed", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent in = new Intent(QuestionActivity.this, PostQuestion.class);
                                startActivity(in);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                finish();
                            }
                        }).show();
            }
        });


        gv = (GridView) findViewById(R.id.list);

        sm = new SessionManager(con);

        aq = new AQuery(con);
        progressDialog = new ProgressDialog(con);

        //new MyAdapter(this, allstds);

        if (!isOnline(con)) {
            Toast.makeText(con, "No network connection", Toast.LENGTH_LONG).show();
        } else {
            sendImage(con);
        }

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub

                Question selectedBus = (Question) parent
                        .getItemAtPosition(position);
                Intent i = new Intent(con, QuestionProfile.class);

                SessionManager speakSession = new SessionManager(con);
                speakSession.set_ques(selectedBus.getQuestion());
                speakSession.set_ques_created(selectedBus.getCreated_at());
                speakSession.set_ques_firstname(selectedBus.getFirstname());
                speakSession.set_ques_surname(selectedBus.getLastname());
                speakSession.set_ques_userphoto(selectedBus.getUserphoto());
                speakSession.set_ques_response(selectedBus.getResponses());
                speakSession.set_ques_response_date(selectedBus.getResponsedate());
                speakSession.set_ques_topic(selectedBus.getTopic());
                // speakSession.set_speakerTopicDesc(selectedBus.getTopic_description());
                startActivity(i);
                overridePendingTransition(R.anim.fade_in
                        , R.anim.fade_out);
                finish();

            }
        });

    }


    class MyAdapter extends BaseAdapter {
        private Context ctx;
        ArrayList<Question> allstd;

        public MyAdapter(Context ctx, ArrayList<Question> stds) {
            this.ctx = ctx;
            allstd = stds;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub

            return allstd.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stublist.get(i);
            return allstd.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        //NB: THIS FUNCTION WILL BE CALLED THE NUMBER OF TIMES RETURNED BY THE getcount METHOD
        @Override
        public View getView(int index, View convertView, ViewGroup parent) {
            //NB : THE FIRST ARGUMENT IS THE INDEX OF THE COLLECTION BEIGN PROCESSED
            //GETTING THE STUDENT OBJECT AT THAT INDEX
            Question st = allstd.get(index);

            // TODO Auto-generated method stub
            //Requesting an inflater from system to dynamically inflate a layout with contents
            LayoutInflater linf = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //inflate fuction returns a view reference to the layout to be inflated
            View v = linf.inflate(R.layout.question_list_item, null);
            if (index % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#f0f0f5"));

            } else {
                v.setBackgroundColor(Color.parseColor("#8e99d7"));
                //holder.age.setTextColor(Color.parseColor("#ffffff"));
            }

            //NB: V IS USED BECAUSE THE VIEWS ARE ON THE the v returned by the inflate method
            ImageView menu_pp = (ImageView) v.findViewById(R.id.ques_user_pp);
            TextView menu_ques = (TextView) v.findViewById(R.id.ques_ques);
            TextView menu_date = (TextView) v.findViewById(R.id.ques_time);
            TextView menu_name = (TextView) v.findViewById(R.id.ques_user);
            TextView menu_answered = (TextView) v.findViewById(R.id.ques_answered);

            //now time to inflate the views on LAYOUT V
            //   use aquery here
            // menu_pp.setImageResource(st.getPhotoId());
            ImageOptions op = new ImageOptions();
            op.targetWidth = 0;
            op.fileCache = true;
            op.animation = AQuery.FADE_IN;
            op.round = 700;

            op.preset = null;
            op.fallback = R.drawable.user;
            op.memCache = true;
            AQuery aq = new AQuery(con);

            //   menu_pp.setImageResource(R.drawable.menuuuu);
            aq.id(menu_pp).image("http://congress.bitberglimited.com/uploads/user/" + st.getUserphoto(),
                    op);
            //aq.im
            menu_ques.setText(st.getQuestion());
            menu_date.setText(st.getCreated_at());
            menu_name.setText(st.getFirstname() + " " + st.getLastname());
//sm.set_ques_response(st.getResponses());

            if (st.getResponses().equalsIgnoreCase("null")) {
                menu_answered.setText("No Answer");
                return v;
            } else {
                menu_answered.setText("Answered");
                return v;
            }

//            return v;
        }
    }
//


    private void sendImage(final Context context) {

        System.out.println("------------------------------------------- " + url);

        /*THIS IS COMMENTED CODE BELOW IS USED TO GET OBJECTS FROM URL
         */
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        aq.progress(progressDialog).ajax(url, JSONArray.class,
                new AjaxCallback<JSONArray>() {
                    @Override
                    public void callback(String url, JSONArray json,
                                         AjaxStatus status) {

                        //AjaxStatus
                        try {
                            if (json != null) {
                                System.out.println("------------------------------------------- " + json + "\n" + status.getCode() + "\n" + status.getMessage());

                                if (status.getCode() == 200) {

                                    for (int i = 0; i < json.length(); i++) {
                                        JSONObject jObject = json.getJSONObject(i);
                                        String ques = jObject.getString("question");
                                        Log.d("ques", ques + "");
                                        String created_at = jObject.getString("created_at");
                                        Log.d("created_at", created_at + "");
                                        String firstname = jObject.getString("first_name");
                                        Log.d("firstname", firstname + "");
                                        String lastname = jObject.getString("last_name");
                                        Log.d("lastname", lastname + "");
                                        String imageName = jObject.getString("user_photo");
                                        Log.d("imageName", imageName + "");
                                        String topic = jObject.getString("topic");
                                        Log.d("topic", topic + "");
                                        String responses = jObject.getString("responses");
                                        Log.d("responses", responses + "");
                                        String response_date = jObject.getString("response_date");
                                        Log.d("response_date", response_date + "");
//                                        String topic_description = jObject.getString("topic_description");
//                                        Log.d("topic_description", topic_description + "");


                                        question = new Question(ques, created_at, firstname, lastname, imageName, topic, responses, response_date);
                            /*
                            PLACING MENU INTO LIST
                                */
                                        allstds.add(question);
                                        // Toast.makeText(con,allstds+"",Toast.LENGTH_LONG).show();

                                        MyAdapter stad = new MyAdapter(con, allstds);
                                        gv.setAdapter(stad);

                                    }

                                } else {
                                    Toast.makeText(con, "Couldn't load menu"
                                            , Toast.LENGTH_LONG).show();
                                }

                            } else {
                                System.out.println("-----------" + json + "" + "--------------------------fail " + "\n" + status.getCode() + "\n" + status.getMessage());
                                Toast.makeText(con, status.getMessage(), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (final Exception ex) {
                            ex.printStackTrace();

                        }

                    }
                });

    }


    private boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }


    @Override
    public void onBackPressed() {
        Intent in = new Intent(getApplicationContext(), WelcomePage.class);
        //   in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }


}
