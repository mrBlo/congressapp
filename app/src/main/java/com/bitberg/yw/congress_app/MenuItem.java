package com.bitberg.yw.congress_app;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yaw on 30/01/2016.
 */
public class MenuItem {

    String name;
    //String age;
    int photoId;

    MenuItem(String name, int photoId) {
        this.name = name;
        //   this.age = age;
        this.photoId = photoId;
    }

    public String getName() {
        return name;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public void setName(String name) {
        this.name = name;
    }

    private List<MenuItem> persons;


    @Override
    public String toString() {
        return " \t" + name;
    }

}