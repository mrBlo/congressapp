package com.bitberg.yw.congress_app;

/**
 * Created by Yaw on 20/03/2016.
 */
public class Question {
    private String question, created_at, firstname, lastname, userphoto, topic, responses, responsedate;

    public Question(String question, String created_at, String firstname, String lastname, String userphoto, String topic, String responses, String responsedate) {
        this.question = question;
        this.created_at = created_at;
        this.firstname = firstname;
        this.lastname = lastname;
        this.userphoto = userphoto;
        this.topic = topic;
        this.responses = responses;
        this.responsedate = responsedate;
    }

    public Question(String question, String created_at, String firstname, String lastname, String userphoto, String responses, String responsedate) {
        this.question = question;
        this.created_at = created_at;
        this.firstname = firstname;
        this.lastname = lastname;
        this.userphoto = userphoto;
        this.responses = responses;
        this.responsedate = responsedate;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUserphoto() {
        return userphoto;
    }

    public void setUserphoto(String userphoto) {
        this.userphoto = userphoto;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getResponses() {
        return responses;
    }

    public void setResponses(String responses) {
        this.responses = responses;
    }

    public String getResponsedate() {
        return responsedate;
    }

    public void setResponsedate(String responsedate) {
        this.responsedate = responsedate;
    }


    @Override
    public String toString() {
        return "Question{" +
                "created_at='" + created_at + '\'' +
                ", question='" + question + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", userphoto='" + userphoto + '\'' +
                ", responses='" + responses + '\'' +
                ", responsedate='" + responsedate + '\'' +
                '}';
    }
}
