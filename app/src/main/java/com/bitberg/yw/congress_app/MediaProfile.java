package com.bitberg.yw.congress_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.ImageOptions;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MediaProfile extends AppCompatActivity {
    ImageView media_Profile_pik;
    TextView media_name, media_imageName, media_created, media_des;
    //String log_firstname_str,log_lastname_str,log_email_str,log_mobile_str;
    SessionManager sm;
    Context con = MediaProfile.this;
    String url = null;
    //+ st.getImage();
    AQuery aq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        media_Profile_pik = (ImageView) findViewById(R.id.media_profile_pik);
        media_name = (TextView) findViewById(R.id.mediaprofile_name);
        media_imageName = (TextView) findViewById(R.id.mediaprofile_imagename);

        media_created = (TextView) findViewById(R.id.mediaprofile_created);
        media_des = (TextView) findViewById(R.id.mediaprofile_desc);

        aq = new AQuery(con);

        sm = new SessionManager(con);
        url = "http://congress.bitberglimited.com/uploads/media/" + sm.get_mediaimagename();
        //  Toast.makeText(con,sm.get_mediaimagename(),Toast.LENGTH_LONG).show();

//        if (sm.get_speakerImage()!=null){
//            AQuery aq=new AQuery(con);-
        ImageOptions op = new ImageOptions();
        op.targetWidth = 0;
        op.fileCache = true;
        op.animation = AQuery.FADE_IN;
        op.round = 700;

        op.preset = null;
        op.fallback = R.drawable.user;
        op.memCache = true;

        if (url.contains("jpg") || url.contains("jpeg") || url.contains("png")) {
            aq.id(media_Profile_pik).image(url, op);
        }
//
//        }
        //    Toast.makeText(con,sm.get_speakerImage(),Toast.LENGTH_SHORT).show();
        media_name.setText("File Name: " + sm.get_medianame());
        media_imageName.setText("File Source: " + sm.get_mediaimagename());
        media_des.setText("Description: " + sm.get_mediadesc());
        media_created.setText("Created On: " + sm.get_mediacreated());


        media_imageName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Download File", Snackbar.LENGTH_LONG)
                        .setAction("Proceed", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                FileDownload();


                            }
                        }).show();

            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Download File", Snackbar.LENGTH_LONG)
                        .setAction("Proceed", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                FileDownload();


                            }
                        }).show();
            }
        });
    }


    public void FileDownload() {
        File Congress = new File(Environment.getExternalStorageDirectory(), "Congress");
        if (!Congress.exists()) {
            Congress.mkdirs();
        }

        ProgressDialog progressDialog = new ProgressDialog(con);
        progressDialog.setMessage("Downloading File..");
        progressDialog.setIndeterminate(false);
        progressDialog.setMax(100);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);

        String filelink = url.toLowerCase();
        if (filelink.contains(".mp3")) {
            File target = new File(Congress, new SimpleDateFormat("dd.MM.yyyy HH.mm").format(new Date()) + ".mp3");
            aq.progress(progressDialog).download(url, target, new AjaxCallback<File>() {
                public void callback(String url, File file, AjaxStatus status) {
                    // Toast.makeText(con,file+"",Toast.LENGTH_LONG).show();
                    if (file != null) {
                        Toast.makeText(con, "Download Complete", Toast.LENGTH_SHORT).show();
                        Toast.makeText(con, "Saved as " + file, Toast.LENGTH_LONG).show();


                    } else {
                        Toast.makeText(con, "Download Failed", Toast.LENGTH_LONG).show();
                    }
                }
            });

        } else if (filelink.contains(".mp4")) {
            File target = new File(Congress, new SimpleDateFormat("dd.MM.yyyy HH.mm").format(new Date()) + ".mp4");
            aq.progress(progressDialog).download(url, target, new AjaxCallback<File>() {
                public void callback(String url, File file, AjaxStatus status) {
                    if (file != null) {
                        Toast.makeText(con, "Download Complete", Toast.LENGTH_SHORT).show();
                        Toast.makeText(con, "Saved as " + file, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(con, "Download Failed", Toast.LENGTH_LONG).show();
                    }
                }
            });

        } else if (filelink.contains(".docx")) {
            File target = new File(Congress, new SimpleDateFormat("dd.MM.yyyy HH.mm").format(new Date()) + ".docx");
            aq.progress(progressDialog).download(url, target, new AjaxCallback<File>() {
                public void callback(String url, File file, AjaxStatus status) {
                    if (file != null) {
                        Toast.makeText(con, "Download Complete", Toast.LENGTH_SHORT).show();
                        Toast.makeText(con, "Saved as " + file, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(con, "Download Failed", Toast.LENGTH_LONG).show();
                    }
                }
            });

        } else if (filelink.contains(".pdf")) {
            File target = new File(Congress, new SimpleDateFormat("dd.MM.yyyy HH.mm").format(new Date()) + ".pdf");
            aq.progress(progressDialog).download(url, target, new AjaxCallback<File>() {
                public void callback(String url, File file, AjaxStatus status) {
                    if (file != null) {
                        Toast.makeText(con, "Download Complete", Toast.LENGTH_SHORT).show();
                        Toast.makeText(con, "Saved as " + file, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(con, "Download Failed", Toast.LENGTH_LONG).show();
                    }
                }
            });

        } else if (filelink.contains(".png")) {
            File target = new File(Congress, new SimpleDateFormat("dd.MM.yyyy HH.mm").format(new Date()) + ".png");
            aq.progress(progressDialog).download(url, target, new AjaxCallback<File>() {
                public void callback(String url, File file, AjaxStatus status) {
                    if (file != null) {
                        Toast.makeText(con, "Download Complete", Toast.LENGTH_SHORT).show();
                        Toast.makeText(con, "Saved as " + file, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(con, "Download Failed", Toast.LENGTH_LONG).show();
                    }
                }
            });

        } else if (filelink.contains(".jpeg")) {
            File target = new File(Congress, new SimpleDateFormat("dd.MM.yyyy HH.mm").format(new Date()) + ".jpeg");
            aq.progress(progressDialog).download(url, target, new AjaxCallback<File>() {
                public void callback(String url, File file, AjaxStatus status) {
                    if (file != null) {
                        Toast.makeText(con, "Download Complete", Toast.LENGTH_SHORT).show();
                        Toast.makeText(con, "Saved as " + file, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(con, "Download Failed", Toast.LENGTH_LONG).show();
                    }
                }
            });

        } else if (filelink.contains(".jpg")) {
            File target = new File(Congress, new SimpleDateFormat("dd.MM.yyyy HH.mm").format(new Date()) + ".jpg");
            aq.progress(progressDialog).download(url, target, new AjaxCallback<File>() {
                public void callback(String url, File file, AjaxStatus status) {
                    if (file != null) {
                        Toast.makeText(con, "Download Complete", Toast.LENGTH_SHORT).show();
                        Toast.makeText(con, "Saved as " + file, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(con, "Download Failed", Toast.LENGTH_LONG).show();
                    }
                }
            });

        } else if (filelink.contains(".gif")) {
            File target = new File(Congress, new SimpleDateFormat("dd.MM.yyyy HH.mm").format(new Date()) + ".gif");
            aq.progress(progressDialog).download(url, target, new AjaxCallback<File>() {
                public void callback(String url, File file, AjaxStatus status) {
                    if (file != null) {
                        Toast.makeText(con, "Download Complete", Toast.LENGTH_SHORT).show();
                        Toast.makeText(con, "Saved as " + file, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(con, "Download Failed", Toast.LENGTH_LONG).show();
                    }
                }
            });

        } else if (filelink.contains(".xlsx")) {
            File target = new File(Congress, new SimpleDateFormat("dd.MM.yyyy HH.mm").format(new Date()) + ".xlsx");
            aq.progress(progressDialog).download(url, target, new AjaxCallback<File>() {
                public void callback(String url, File file, AjaxStatus status) {
                    if (file != null) {
                        Toast.makeText(con, "Download Complete", Toast.LENGTH_SHORT).show();
                        Toast.makeText(con, "Saved as " + file, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(con, "Download Failed", Toast.LENGTH_LONG).show();
                    }
                }
            });

        } else if (filelink.contains(".flv")) {
            File target = new File(Congress, new SimpleDateFormat("dd.MM.yyyy HH.mm").format(new Date()) + ".flv");
            aq.progress(progressDialog).download(url, target, new AjaxCallback<File>() {
                public void callback(String url, File file, AjaxStatus status) {
                    if (file != null) {
                        Toast.makeText(con, "Download Complete", Toast.LENGTH_SHORT).show();
                        Toast.makeText(con, "Saved as " + file, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(con, "Download Failed", Toast.LENGTH_LONG).show();
                    }
                }
            });

        } else {
            Toast.makeText(con, "Unknown File Format", Toast.LENGTH_SHORT).show();
        }


    }


    @Override
    public void onBackPressed() {
        Intent in = new Intent(getApplicationContext(), MediaActivity.class);
        //   in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }

}
