package com.bitberg.yw.congress_app;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;

public class ParticipantProfile extends AppCompatActivity {
    ImageView part_Profile_pik;
    TextView part_name, part_imageName, part_type, part_school, part_level, part_phone;

    SessionManager sm;
    Context con = ParticipantProfile.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_participant_profile);
        part_Profile_pik = (ImageView) findViewById(R.id.parti_profile_pik);
        part_name = (TextView) findViewById(R.id.parti_profile_name);
        part_type = (TextView) findViewById(R.id.parti_type);

        part_school = (TextView) findViewById(R.id.parti_profile_school);
        part_level = (TextView) findViewById(R.id.parti_profile_year);
        part_phone = (TextView) findViewById(R.id.parti_profile_phone);

        sm = new SessionManager(con);
        ImageOptions op = new ImageOptions();
        op.targetWidth = 0;
        op.fileCache = true;
        op.animation = AQuery.FADE_IN;
        //op.round=100;
        op.preset = null;
        op.fallback = R.drawable.user;
        op.memCache = false;


        AQuery aq = new AQuery(con);
        aq.id(part_Profile_pik).image("http://congress.bitberglimited.com/uploads/user/" + sm.get_partimage(),
                op);


        part_name.setText(sm.get_partfname() + " " + sm.get_partsurname());
        part_type.setText(sm.get_parttype());
        part_school.setText(sm.get_partschool());
        part_level.setText("Level: " + sm.get_partyear());
        part_phone.setText("Phone: " + sm.get_partphone());


    }


    @Override
    public void onBackPressed() {
        Intent in = new Intent(getApplicationContext(), ParticipantsActivity.class);
        //   in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }


}
