package com.bitberg.yw.congress_app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class WelcomePage extends AppCompatActivity {
    RecyclerView rv;
    ArrayList<MenuItem> allstds = new ArrayList<MenuItem>();
    GridView gv;
    Context con = WelcomePage.this;
    CollapsingToolbarLayout collapsingToolbarLayout;
    SessionManager sm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_page);
        gv = (GridView) findViewById(R.id.list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sm = new SessionManager(con);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout11);
        collapsingToolbarLayout.setTitle("Congress");

//        if (sm.get_is_logged_in()!=true){
//            Toast.makeText(con,"You are logged out",Toast.LENGTH_SHORT).show();
//            Intent in = new Intent(con, Login.class);
//            startActivity(in);
//            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//            finish();
//        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "View Profile", Snackbar.LENGTH_LONG)
                        .setAction("Proceed", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent in = new Intent(WelcomePage.this, EditProfile.class);
                                startActivity(in);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                finish();
                            }
                        }).show();
            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Do you want to logout?", Snackbar.LENGTH_LONG)
                        .setAction("Proceed", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                sm.set_is_logged_in(false);
                                Intent in = new Intent(WelcomePage.this, Login.class);
                                startActivity(in);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                finish();
                            }
                        }).show();
            }
        });


        allstds.add(new MenuItem("Congress Outline", R.drawable.navlineup));
        allstds.add(new MenuItem("Speakers", R.drawable.speakers));
        allstds.add(new MenuItem("Question Board", R.drawable.comments));
        allstds.add(new MenuItem("Food Menu", R.drawable.menuu));
        allstds.add(new MenuItem("Media", R.drawable.multimediaicon));
        allstds.add(new MenuItem("Participants", R.drawable.users));
        allstds.add(new MenuItem("Notable Topics", R.drawable.notifications));

        MyAdapter stad = new MyAdapter(this, allstds);
        gv.setAdapter(stad);

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                // TODO Auto-generated method stub
                MenuItem mem = (MenuItem) parent.getItemAtPosition(position);
                final String selectName = mem.getName();
                Log.d("WATCH THIS", selectName);
                if (selectName.equals("Question Board")) {
                    Intent in = new Intent(con, QuestionActivity.class);
                    startActivity(in);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();

                } else if (selectName.equals("Food Menu")) {
////
                    Intent in = new Intent(con, FoodMenu.class);
                    startActivity(in);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();

                } else if (selectName.equals("Media")) {
                    Intent in = new Intent(con, MediaActivity.class);
                    startActivity(in);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                } else if (selectName.equals("Participants")) {
////
                    Intent in = new Intent(con, ParticipantsActivity.class);
                    startActivity(in);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();

                } else if (selectName.equals("Speakers")) {
                    Intent in = new Intent(con, SpeakersActivity.class);
                    startActivity(in);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Select Valid Input", Toast.LENGTH_LONG).show();
                }


            }
        });


    }

    class MyAdapter extends BaseAdapter {
        private Context ctx;
        ArrayList<MenuItem> allstd;

        public MyAdapter(Context ctx, ArrayList<MenuItem> stds) {
            this.ctx = ctx;
            allstd = stds;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub

            return allstd.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stublist.get(i);
            return allstd.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }


        //NB: THIS FUNCTION WILL BE CALLED THE NUMBER OF TIMES RETURNED BY THE getcount METHOD
        @Override
        public View getView(int index, View convertView, ViewGroup parent) {
            //NB : THE FIRST ARGUMENT IS THE INDEX OF THE COLLECTION BEIGN PROCESSED
            //GETTING THE STUDENT OBJECT AT THAT INDEX
            MenuItem st = allstd.get(index);


            // TODO Auto-generated method stub
            //Requesting an inflater from system to dynamically inflate a layout with contents
            LayoutInflater linf = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //inflate fuction returns a view reference to the layout to be inflated
            View v = linf.inflate(R.layout.list_item, null);
            if (index % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#f0f0f5"));

            } else {
                v.setBackgroundColor(Color.parseColor("#8e99d7"));
                //holder.age.setTextColor(Color.parseColor("#ffffff"));
            }

            //NB: V IS USED BECAUSE THE VIEWS ARE ON THE the v returned by the inflate method
            ImageView photo = (ImageView) v.findViewById(R.id.pp);
            TextView details = (TextView) v.findViewById(R.id.stdinfo);

            //now time to inflate the views on LAYOUT V
            photo.setImageResource(st.getPhotoId());
            ;
            details.setText(st.toString());


            return v;
        }


    }

    @Override
    public void onBackPressed() {
        Intent in = new Intent(getApplicationContext(), Login.class);
        //   in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }
}
