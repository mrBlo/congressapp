package com.bitberg.yw.congress_app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class RegistrationType extends AppCompatActivity {
    // MyCustomAdapter dataAdapter = null;
    ListView listView;
    ArrayList<MenuItem> allstds = new ArrayList<MenuItem>();
    GridView gv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_type);
        // listView = (ListView) findViewById(R.id.list);
        gv = (GridView) findViewById(R.id.list);

        allstds.add(new MenuItem("Student", R.drawable.studen));
        allstds.add(new MenuItem("Alumni", R.drawable.alumni));
        allstds.add(new MenuItem("Visitor", R.drawable.visito));
        MyAdapter stad = new MyAdapter(this, allstds);
        gv.setAdapter(stad);

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                // TODO Auto-generated method stub
                MenuItem mem = (MenuItem) parent.getItemAtPosition(position);
                final String selectName = mem.getName();
                Log.d("WATCH THIS", selectName);
                if (selectName.equals("Student")) {
                    Intent in = new Intent(RegistrationType.this, Student_Registration.class);
                    startActivity(in);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();

                } else if (selectName.equals("Alumni")) {
//
                    Intent in = new Intent(RegistrationType.this, Alumni_Registration.class);
                    startActivity(in);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();

                } else if (selectName.equals("Visitor")) {
                    Intent in = new Intent(RegistrationType.this, Visitor_Registration.class);
                    startActivity(in);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Select Valid Input", Toast.LENGTH_LONG).show();
                }


            }
        });


    }

    class MyAdapter extends BaseAdapter {
        private Context ctx;
        ArrayList<MenuItem> allstd;

        public MyAdapter(Context ctx, ArrayList<MenuItem> stds) {
            this.ctx = ctx;
            allstd = stds;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub

            return allstd.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stublist.get(i);
            return allstd.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }


        //NB: THIS FUNCTION WILL BE CALLED THE NUMBER OF TIMES RETURNED BY THE getcount METHOD
        @Override
        public View getView(int index, View convertView, ViewGroup parent) {
            //NB : THE FIRST ARGUMENT IS THE INDEX OF THE COLLECTION BEIGN PROCESSED
            //GETTING THE STUDENT OBJECT AT THAT INDEX
            MenuItem st = allstd.get(index);


            // TODO Auto-generated method stub
            //Requesting an inflater from system to dynamically inflate a layout with contents
            LayoutInflater linf = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //inflate fuction returns a view reference to the layout to be inflated
            View v = linf.inflate(R.layout.list_item, null);
            if (index % 2 == 0) {
                v.setBackgroundColor(Color.parseColor("#f0f0f5"));

            } else {
                v.setBackgroundColor(Color.parseColor("#8e99d7"));
                //holder.age.setTextColor(Color.parseColor("#ffffff"));
            }

            //NB: V IS USED BECAUSE THE VIEWS ARE ON THE the v returned by the inflate method
            ImageView photo = (ImageView) v.findViewById(R.id.pp);
            TextView details = (TextView) v.findViewById(R.id.stdinfo);

            //now time to inflate the views on LAYOUT V
            photo.setImageResource(st.getPhotoId());
            details.setText(st.toString());


            return v;
        }


    }

    @Override
    public void onBackPressed() {
        Intent in = new Intent(getApplicationContext(), Login.class);
        //   in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }
}