package com.bitberg.yw.congress_app;

/**
 * Created by Yaw on 10/03/2016.
 */
public class Speaker {
    private String id, imageName, biography, website, email, phone_no, name, topic, topic_description;

    public Speaker(String id, String imageName, String biography, String website, String email, String phone_no, String name, String topic, String topic_description) {
        this.id = id;
        this.imageName = imageName;
        this.biography = biography;
        this.website = website;
        this.email = email;
        this.phone_no = phone_no;
        this.name = name;
        this.topic = topic;
        this.topic_description = topic_description;
    }

    public Speaker(String id, String imageName, String phone_no, String name, String topic) {
        this.id = id;
        this.imageName = imageName;
        this.phone_no = phone_no;
        this.name = name;
        this.topic = topic;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTopic_description() {
        return topic_description;
    }

    public void setTopic_description(String topic_description) {
        this.topic_description = topic_description;
    }


    @Override
    public String toString() {
        return
                "email='" + email + "\n" +
                        "phone_no='" + phone_no + "\n" +
                        "name='" + name + "\n" +
                        "topic='" + topic + "\n" +
                        "topic_description='" + topic_description + "\n" +
                        "biography='" + biography + "\n" +
                        "website='" + website
                ;
    }
}
