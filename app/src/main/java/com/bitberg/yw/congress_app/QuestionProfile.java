package com.bitberg.yw.congress_app;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;

public class QuestionProfile extends AppCompatActivity {
    ImageView pp;
    TextView ques, created, askedby, response, responsedate;
    SessionManager sm;
    Context con = QuestionProfile.this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_profile);

        pp = (ImageView) findViewById(R.id.ques_profile_pp);
        ques = (TextView) findViewById(R.id.ques_profile_ques);
        askedby = (TextView) findViewById(R.id.ques_profile_askedby);

        response = (TextView) findViewById(R.id.ques_profile_response);
        responsedate = (TextView) findViewById(R.id.ques_profile_responsedate);
        created = (TextView) findViewById(R.id.ques_profile_date);
//        speaker_topic= (TextView) findViewById(R.id.speakerprofile_topic);

        sm = new SessionManager(con);
//        if (sm.get_speakerImage()!=null){

        ImageOptions op = new ImageOptions();
        op.targetWidth = 0;
        op.fileCache = true;
        op.animation = AQuery.FADE_IN;
//        op.round=700;

        op.preset = null;
        op.fallback = R.drawable.user;
        op.memCache = true;


        AQuery aq = new AQuery(con);
        aq.id(pp).image("http://congress.bitberglimited.com/uploads/user/" + sm.get_ques_userphoto(), op);

        // }
        //    Toast.makeText(con,sm.get_speakerImage(),Toast.LENGTH_SHORT).show();
        ques.setText("Question: " + sm.get_ques());
        if (sm.get_ques_surname().equalsIgnoreCase("null")) {
            askedby.setText("Asked by: User Not Specified");

        } else {
            askedby.setText("Asked by: " + sm.get_ques_surname() + " " + sm.get_ques_firstname());

        }
        created.setText(sm.get_ques_created());
        if (sm.get_ques_response().equalsIgnoreCase("null")) {
            response.setText("Not Answered");
            responsedate.setText("");

        } else {
            response.setText("Answer: " + sm.get_ques_response());
            responsedate.setText("Answered on: " + sm.get_ques_response_date());

        }


    }


    @Override
    public void onBackPressed() {
        Intent in = new Intent(getApplicationContext(), QuestionActivity.class);
        //   in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }


}
