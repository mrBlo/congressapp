package com.bitberg.yw.congress_app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;

public class SpeakerProfile extends AppCompatActivity {
    ImageView speaker_Profile_pik;
    TextView speaker_name, speaker_imageName, speaker_bio, speaker_website, speaker_email, speaker_phone, speaker_topic, speaker_topicdes;
    //String log_firstname_str,log_lastname_str,log_email_str,log_mobile_str;
    SessionManager sm;
    Context con = SpeakerProfile.this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speaker_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        speaker_Profile_pik = (ImageView) findViewById(R.id.speaker_profile_pik);
        speaker_name = (TextView) findViewById(R.id.speakerprofile_name);
        speaker_bio = (TextView) findViewById(R.id.speakerprofile_bio);

        speaker_website = (TextView) findViewById(R.id.speakerprofile_website);
        speaker_email = (TextView) findViewById(R.id.speakerprofile_email);
        speaker_phone = (TextView) findViewById(R.id.speakerprofile_phone);
        speaker_topic = (TextView) findViewById(R.id.speakerprofile_topic);
        speaker_topicdes = (TextView) findViewById(R.id.speakerprofile_topicdes);

        sm = new SessionManager(con);
        if (sm.get_speakerImage() != null) {
            AQuery aq = new AQuery(con);
            aq.id(speaker_Profile_pik).image("http://congress.bitberglimited.com/uploads/speakers/" + sm.get_speakerImage(), false, true, 0, R.drawable.user, null, AQuery.FADE_IN);

        }
        //    Toast.makeText(con,sm.get_speakerImage(),Toast.LENGTH_SHORT).show();
        speaker_name.setText(sm.get_speakerName());
        speaker_bio.setText(sm.get_speakerbio());
        speaker_website.setText(sm.get_speakerwebsite());
        speaker_email.setText(sm.get_speakerEmail());
        speaker_phone.setText("Phone: " + sm.get_speakerPhone());
        speaker_topic.setText("Topic: " + sm.get_speakerTopic());
        speaker_topicdes.setText("Topic Description:\n " + sm.get_speakerTopicDesc());


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Send Speaker a message", Snackbar.LENGTH_LONG)
                        .setAction("Proceed", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
//                                Intent in = new Intent(Login.this, RegistrationType.class);
//                                startActivity(in);
//                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                                finish();
                            }
                        }).show();
            }
        });
    }


    @Override
    public void onBackPressed() {
        Intent in = new Intent(getApplicationContext(), SpeakersActivity.class);
        //   in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }


}
