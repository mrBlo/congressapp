package com.bitberg.yw.congress_app;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;

import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Student_Registration extends AppCompatActivity {
    User student;
    EditText surnameET, firstnameET, othernamesET, phoneET, emailET, schoolET,
            yearET, programmeET, local_congregationET, locationET, passwordET, repeat_passwordET;
    RadioGroup genderRG, church_memberRG, allergiesRG;
    String password, repeat_password,
            surname, firstname, othernames,
            phone, email, school, year, programme,
            allergies, local_congregation,
            location, gender, church_member;

    private static String url = "http://congress.bitberglimited.com/api/regions.json";
    //private static String url = "http://www.google.com/uds/GnewsSearch?q=Obama&v=1.0";
//private static String url = "http://api.geonames.org/citiesJSON?north=44.1&south=-9.9&east=-22.4&west=55.2&lang=de&username=demo";
//    private static String url = "http://www.google.com";
    Uri fileUri;
    Bitmap photo;
    int CAMERA_REQUEST = 100;
    private int PICK_IMAGE_REQUEST = 1;
    private static final String TAG_SUCCESS = "success";
    SessionManager sm;
    File image = null;
    String filePath = "";
    FloatingActionButton fab2;
    ProgressDialog progressDialog;
    AlertDialog.Builder builder;

    AQuery aq;
    CollapsingToolbarLayout collapsingToolbarLayout;
    ImageView profile_pik, dialog_profile_pik;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student__registration);
        profile_pik = (ImageView) findViewById(R.id.profile_pik);
        sm = new SessionManager(Student_Registration.this);
        aq = new AQuery(Student_Registration.this);
        progressDialog = new ProgressDialog(Student_Registration.this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        //  pik = (Bitmap)getIntent().getParcelableExtra("pik");

        if (sm.getIMG() != null) {
            // Toast.makeText(Student_Registration.this,"Invalid image",Toast.LENGTH_LONG).show();
            Bitmap bitmap = BitmapFactory.decodeFile(sm.getIMG());
            //Bitmap.createScaledBitmap(bitmap,(int)(bitmap.getWidth()*0.8),(int)(bitmap.getHeight()*0.8),true);
            profile_pik.setImageBitmap(bitmap);
        }

//            Bitmap bitmap = BitmapFactory.decodeFile(sm.getIMG());
//            profile_pik.setImageBitmap(bitmap);


        collapsingToolbarLayout = new CollapsingToolbarLayout(Student_Registration.this);

//EditTexts and Radio Groups
        passwordET = (EditText) findViewById(R.id.input_password);
        repeat_passwordET = (EditText) findViewById(R.id.input_repeat_password);

        surnameET = (EditText) findViewById(R.id.input_surname);
        firstnameET = (EditText) findViewById(R.id.input_firstname);
        othernamesET = (EditText) findViewById(R.id.input_othernames);
        phoneET = (EditText) findViewById(R.id.input_phone);
        emailET = (EditText) findViewById(R.id.input_email);
        schoolET = (EditText) findViewById(R.id.input_school);
        yearET = (EditText) findViewById(R.id.input_year_of_completion);
        programmeET = (EditText) findViewById(R.id.input_programme);
        // allergiesET= (EditText) findViewById(R.id.input_food_allergy);
        local_congregationET = (EditText) findViewById(R.id.input_local_congregation);
        locationET = (EditText) findViewById(R.id.location);

        genderRG = (RadioGroup) findViewById(R.id.gender_radiogroup);
        church_memberRG = (RadioGroup) findViewById(R.id.church_radiogroup);
        allergiesRG = (RadioGroup) findViewById(R.id.allergies_radiogroup);


//setting session manager to edittexts
        surnameET.setText(sm.getSurname());
        firstnameET.setText(sm.getFirstname());
        othernamesET.setText(sm.getothername());
        phoneET.setText(sm.getphone());
        emailET.setText(sm.getemail());
        schoolET.setText(sm.getschool());
        yearET.setText(sm.getyear());
        programmeET.setText(sm.getprog());
        locationET.setText(sm.getlocation());
        local_congregationET.setText(sm.getlocalcongregation());
        passwordET.setText(sm.getpassword());
        repeat_passwordET.setText(sm.getpasswordalt());


        //RadioGroup onClicks
        genderRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup arg0, int arg1) {
                // TODO Auto-generated method stub

                switch (arg1) {
                    case R.id.gender_male:
                        gender = "Male";
                        break;

                    case R.id.gender_female:
                        gender = "Female";
                        break;
                }
            }
        });

        church_memberRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup arg0, int arg1) {
                // TODO Auto-generated method stub

                switch (arg1) {
                    case R.id.church_member_yes:
                        church_member = "true";
                        break;

                    case R.id.church_member_no:
                        church_member = "false";
                        break;
                }
            }
        });

        allergiesRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup arg0, int arg1) {
                // TODO Auto-generated method stub

                switch (arg1) {
                    case R.id.allergies_yes:
                        allergies = "true";
                        break;

                    case R.id.allergies_no:
                        allergies = "false";
                        break;
                }
            }
        });


//Other find by IDs
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        Student_Registration.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //collapsingToolbarLayout= (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);

        //  dialog_profile_pik= (ImageView) findViewById(R.id.dialog_profile_pik);


        //Registration Button
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Do you want to save this registration", Snackbar.LENGTH_LONG)
                        .setAction("Proceed", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
//                            Intent in=new Intent(Student_Registration.this, RegistrationType.class);
//                            startActivity(in);
//                            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
//                            finish();
                                //Setting Strings to EditTexts
                                password = passwordET.getText().toString();
                                repeat_password = repeat_passwordET.getText().toString();

                                //Validation
                                validate();

                            }
                        }).show();
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab1);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "..to Camera", Snackbar.LENGTH_LONG)
//                        .setAction("Proceed", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
                builder = new AlertDialog.Builder(Student_Registration.this);
                //  builder.setMessage("Select");
                builder.setTitle("Profile Image");
                builder.setCancelable(true);

                builder.setIcon(android.R.drawable.ic_menu_camera);
                builder.setPositiveButton("Take photo", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        //	stopService(svc);
                        try {

                            File imagesFolder = new File(Environment.getExternalStorageDirectory(), "/capture_img/");
                            if (!imagesFolder.exists()) {
                                imagesFolder.mkdirs();
                            }
                            Long tsLong = System.currentTimeMillis() / 1000;
                            String ts = tsLong.toString();
                            image = new File(imagesFolder, "bitberg_" + ts + ".jpg");
                            fileUri = Uri.fromFile(image);
                            Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                    i.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                            startActivityForResult(i, CAMERA_REQUEST);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });

                builder.setNegativeButton("Choose existing", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        Intent intent = new Intent();
                        //shows only images,no vids
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        //Always show chooser
                        startActivityForResult(Intent.createChooser(intent, "Select"), PICK_IMAGE_REQUEST);

                    }
                });
                AlertDialog alert = builder.create();
                alert.show();


            }
        });

        profile_pik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(), FullscreenActivity.class);
                if (sm.getIMGfilepath() == null) {
                    Toast.makeText(Student_Registration.this, "Profile Picture Unavailable", Toast.LENGTH_SHORT).show();
                } else {
                    sm.setIMG(sm.getIMGfilepath());
//storing to preferences
                    sm.setSurname(surnameET.getText().toString());
                    sm.setFirstname(firstnameET.getText().toString());
                    sm.setothername(othernamesET.getText().toString());
                    sm.setphone(phoneET.getText().toString());
                    sm.setemail(emailET.getText().toString());
                    sm.setschool(schoolET.getText().toString());
                    sm.setyear(yearET.getText().toString());
                    sm.setpro(programmeET.getText().toString());
                    //  sm.setgender(genderRG.getCheckedRadioButtonId().getText().toString());
                    sm.setlocalcongregation(local_congregationET.getText().toString());
                    sm.setlocation(locationET.getText().toString());
                    sm.setpassword(passwordET.getText().toString());
                    sm.setpasswordalt(repeat_passwordET.getText().toString());


                    startActivity(in);

                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                }
            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        // super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            try {
                photo = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                Uri currImageURI = getImageUri(getApplicationContext(), photo);
                filePath = getRealPathFromURI(currImageURI);

                //my sessionmanagers stores filepath
                sm.setIMGfilepath(filePath);

                profile_pik.setImageBitmap(photo);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(Student_Registration.this, "file not found", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            // super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
//
                photo = (Bitmap) data.getExtras().get("data");

                Uri currImageURI = getImageUri(getApplicationContext(), photo);

                filePath = getRealPathFromURI(currImageURI);

                //////
                sm.setIMGfilepath(filePath);
//                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                photo.compress(Bitmap.CompressFormat.JPEG, 75, bytes);


                profile_pik.setImageBitmap(photo);
                //  Toast.makeText(Student_Registration.this,filePath,Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        Toast.makeText(Student_Registration.this,"onPause method",Toast.LENGTH_SHORT).show();
//        sm.setIMG(null);
//        sm.setIMGfilepath(null);
//
//    }

    private boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }


    public String getRealPathFromURI(Uri contentUri) {
        // can post image
        String[] proj = {MediaStore.MediaColumns.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 75, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    public void validate() {
        boolean valid = true;

        password = passwordET.getText().toString();
        repeat_password = repeat_passwordET.getText().toString();

        surname = surnameET.getText().toString();
        firstname = firstnameET.getText().toString();
        othernames = othernamesET.getText().toString();
        email = emailET.getText().toString();
        phone = phoneET.getText().toString();
        school = schoolET.getText().toString();
        programme = programmeET.getText().toString();
        year = yearET.getText().toString();
        // allergies = allergiesET.getText().toString();
        local_congregation = local_congregationET.getText().toString();
        location = locationET.getText().toString();


        if (!isOnline(Student_Registration.this)) {
            Toast.makeText(Student_Registration.this, "No network connection", Toast.LENGTH_LONG).show();
        } else if (surname.isEmpty()) {
            Toast.makeText(Student_Registration.this, "Empty surname field", Toast.LENGTH_LONG).show();
            valid = false;
        } else if (firstname.isEmpty()) {
            Toast.makeText(Student_Registration.this, "Empty firstname field", Toast.LENGTH_LONG).show();
            valid = false;
        } else if (phone.isEmpty() || phone.length() != 10) {
            Toast.makeText(Student_Registration.this, "Invalid phone number", Toast.LENGTH_LONG).show();
            phoneET.setError("enter valid phone number");
            valid = false;
        } else if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(Student_Registration.this, "Invalid email address", Toast.LENGTH_LONG).show();
            emailET.setError("enter valid email address");
            valid = false;
        } else if (password.isEmpty()) {
            passwordET.setError("Empty password field");
            valid = false;
            Toast.makeText(Student_Registration.this, "Empty password field", Toast.LENGTH_LONG).show();
        } else if (repeat_password.isEmpty()) {
            repeat_passwordET.setError("Empty repeat password field");
            valid = false;
            Toast.makeText(Student_Registration.this, "Empty repeat password field", Toast.LENGTH_LONG).show();
        } else if (password.length() < 6 || password.length() > 20) {
            passwordET.setError("between 6 and 20 alphanumeric characters");
            valid = false;
        } else if (repeat_password.length() < 6 || repeat_password.length() > 20) {
            repeat_passwordET.setError("between 6 and 20 alphanumeric characters");
            valid = false;
        } else if (!repeat_password.matches(password)) {
            Toast.makeText(Student_Registration.this, "Passwords do not match", Toast.LENGTH_LONG).show();
            valid = false;
        }


////////////////////////////////////////////////////////////////////////////
        else if (gender == null) {
            Toast.makeText(Student_Registration.this, "Empty gender field", Toast.LENGTH_LONG).show();
            valid = false;
        } else if (church_member == null) {
            Toast.makeText(Student_Registration.this, "Empty church member field", Toast.LENGTH_LONG).show();
            valid = false;
        } else if (allergies == null) {
            Toast.makeText(Student_Registration.this, "Empty allergy field", Toast.LENGTH_LONG).show();
            valid = false;
        } else if (school.isEmpty()) {
            Toast.makeText(Student_Registration.this, "Empty school field", Toast.LENGTH_LONG).show();
            //  emailET.setError("enter valid email address");
            valid = false;
        } else if (year.isEmpty() || year.length() != 4) {
            Toast.makeText(Student_Registration.this, "Invalid year of completion", Toast.LENGTH_LONG).show();
            //  emailET.setError("enter valid email address");
            valid = false;
        } else if (programme.isEmpty()) {
            Toast.makeText(Student_Registration.this, "Empty programme field", Toast.LENGTH_LONG).show();
            //  emailET.setError("enter valid email address");
            valid = false;
        } else {
//setting null for empty non required params
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (location.isEmpty() || location == null) {
                location = "NULL";

            }
            if (local_congregation.isEmpty() || local_congregation == null) {
                local_congregation = "NULL";
            }

            if (othernames.isEmpty() || othernames == null) {
                othernames = "NULL";
            }
            //  sm.setUsername(phone);

            Toast.makeText(Student_Registration.this, location + "\n" + local_congregation + "\n" + othernames, Toast.LENGTH_SHORT).show();

            if (!isOnline(Student_Registration.this)) {
                Toast.makeText(Student_Registration.this, "No network connection", Toast.LENGTH_LONG).show();
            }
            if (sm.getIMGfilepath() == null)
            //(photo == null)
            {
                Toast.makeText(Student_Registration.this, "No picture set", Toast.LENGTH_LONG).show();
            }
            //else {
//                Toast.makeText(Student_Registration.this, "Registering..", Toast.LENGTH_LONG).show();


            sm.setUsername(phone);
            //Sending details
            sendImage(Student_Registration.this, surname, firstname, othernames,
                    phone, email, school, year, programme,
                    gender, allergies, church_member,
                    local_congregation, location, password, sm.getIMGfilepath(), "Student");
        }


    }

    // return valid;
    //}


    private void sendImage(final Context context, final String surname_, final String firstname_, final String othernames_, final String phone_,
                           final String email_, final String school_, final String year_, final String programme_,
                           final String gender_, final String allergies_, final String churchMember_, final String localCongregation_,
                           final String location_, final String password_, final String image_, final String participantType_) {


        //Toast.makeText(Student_Registration.this, "pt 2", Toast.LENGTH_LONG).show();
        // Log.e("-///////////----",image_);
        //  ConnectionDetector cd=new ConnectionDetector(context);
        //  if(cd.isConnectingToInternet()){//chang this function later
        File fileUri2 = new File(image_);
        if (fileUri2.isFile()) {
            System.out.println("------------------------------------------- file");
        } else {
            System.out.println("------------------------------------------- not file");
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("password", password_);
        params.put("firstName", firstname_);
        params.put("lastName", surname_);
        params.put("otherName", othernames_);
        params.put("mobileNo", phone_);
        params.put("email", email_);
        params.put("gender", gender_);
        params.put("school", school_);
        params.put("participantType", participantType_);
        params.put("level", "Null");
        params.put("yearCompletion", year_);
        params.put("expectedYearCompletion", "Null");
        params.put("coursePursued", programme_);
        params.put("foodAllergies", allergies_);
        params.put("churchMember", churchMember_);
        params.put("localCongregation", localCongregation_);
        params.put("location", location_);
//        params.put("region", "Null");
//        params.put("district", "Null");
//        params.put("town", "Null");
        params.put("imageName", fileUri2);
        // JSONObject json = jsonParser.makeHttpRequest(url,"POST", params);
        System.out.println("------------------------------------------- " + params);


        System.out.println("------------------------------------------- " + fileUri2);
        System.out.println("------------------------------------------- " + url);

        /*THIS IS COMMENTED CODE BELOW IS USED TO GET OBJECTS FROM URL
         */
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
//        aq.progress(progressDialog).ajax(url, JSONObject.class, new AjaxCallback<JSONObject>() {
//            @Override
//            public void callback(String url, JSONObject object, AjaxStatus status) {
//                if (object != null) {
//                    System.out.println("------------------------------------------- " + object.toString() + "\n" + status.getCode());
//                    Toast.makeText(Student_Registration.this,status.getMessage(),Toast.LENGTH_SHORT).show();
//                } else {
//                    System.out.println("-------------------------------------fail " + "\n" + status.getCode());
//                    Toast.makeText(Student_Registration.this,status.getMessage(),Toast.LENGTH_SHORT).show();
//                }
//            }
//        });


        aq.progress(progressDialog).ajax(url, params, JSONObject.class,
                new AjaxCallback<JSONObject>() {
                    @Override
                    public void callback(String url, JSONObject json,
                                         AjaxStatus status) {

                        //AjaxStatus
                        try {
                            //you can format any json string in the php and you can get it in the json Object
                            //example
                            if (json != null) {
                                System.out.println("------------------------------------------- " + json + "\n" + status.getCode() + "\n" + status.getMessage());
                                int success = json.getInt(TAG_SUCCESS);
                                Log.e("++++++++++ ", success + "\n" + json.getString("message"));
                            } else {
                                System.out.println("-----------" + json + "" + "--------------------------fail " + "\n" + status.getCode() + "\n" + status.getMessage());
                            }
//                            if(success==1){
//                                Toast.makeText(Student_Registration.this, json.getString("message"), Toast.LENGTH_LONG).show();
//                                // System.out.println("------------------------------------------- part 3");
//
////                                Intent i = new Intent(getApplicationContext(),WelcomeActivity.class);
////
////                                i.putExtra("username",user_id);
////                                // sm.setUSERNAME(username);
////                                i.putExtra("password",sm.getPASSWORD());
////
////                                startActivity(i);
////                                overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);
////                                finish();
//                            }else{
//                                Toast.makeText(Student_Registration.this, json.getString("message"), Toast.LENGTH_LONG).show();
//                            }

                        }
//                        catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                        catch (final Exception ex) {
                            ex.printStackTrace();

                        }

                    }
                });


    }


    @Override
    public void onBackPressed() {
        Intent in = new Intent(getApplicationContext(), RegistrationType.class);
        //   in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(in);
        sm.setIMG(null);
        sm.setIMGfilepath(null);

        //
        sm.setSurname(null);
        sm.setFirstname(null);
        sm.setothername(null);
        sm.setphone(null);

        sm.setemail(null);
        sm.setschool(null);
        sm.setyear(null);
        sm.setpro(null);
        sm.setgender(null);
        sm.setfoodallergies(null);
        sm.setchurchmember(null);
        sm.setlocation(null);

        sm.setlocalcongregation(null);
        sm.setpassword(null);
        sm.setpasswordalt(null);
        //
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();

    }


}

