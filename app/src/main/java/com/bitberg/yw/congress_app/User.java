package com.bitberg.yw.congress_app;

/**
 * Created by Yaw on 7/02/2016.
 */
public class User {
    private String image, id, surname, firstname,
            othername, phone, email,
            school, year, programme, participant_type,
            gender, allergies, church_member, local_congregation, location,
            password;
    private String username;

    public User() {
    }

    public User(String image, String id, String surname, String firstname, String othername, String phone, String school, String year, String participant_type) {
        this.image = image;
        this.id = id;
        this.surname = surname;
        this.firstname = firstname;
        this.othername = othername;
        this.phone = phone;
        this.school = school;
        this.year = year;
        this.participant_type = participant_type;
    }

    public String getParticipant_type() {
        return participant_type;
    }

    public void setParticipant_type(String participant_type) {
        this.participant_type = participant_type;
    }

    //Pre Registration User w/o Username
    public User(String image, String surname, String firstname, String othername,
                String phone, String email, String school, String year,
                String programme, String gender, String allergies, String church_member,
                String local_congregation, String location, String password) {
        this.image = image;
        this.surname = surname;
        this.firstname = firstname;
        this.othername = othername;
        this.phone = phone;
        this.email = email;
        this.school = school;
        this.year = year;
        this.programme = programme;
        this.gender = gender;
        this.allergies = allergies;
        this.church_member = church_member;
        this.local_congregation = local_congregation;
        this.location = location;
        this.password = password;
    }

    //Post Registration with Username
    public User(String image, String surname, String firstname, String othername,
                String phone, String email, String school, String year,
                String programme, String gender, String allergies, String church_member,
                String local_congregation, String location, String password, String username) {
        this.image = image;
        this.surname = surname;
        this.firstname = firstname;
        this.othername = othername;
        this.phone = phone;
        this.email = email;
        this.school = school;
        this.year = year;
        this.programme = programme;
        this.gender = gender;
        this.allergies = allergies;
        this.church_member = church_member;
        this.local_congregation = local_congregation;
        this.location = location;
        this.password = password;
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getOthername() {
        return othername;
    }

    public void setOthername(String othername) {
        this.othername = othername;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getProgramme() {
        return programme;
    }

    public void setProgramme(String programme) {
        this.programme = programme;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    public String getChurch_member() {
        return church_member;
    }

    public void setChurch_member(String church_member) {
        this.church_member = church_member;
    }

    public String getLocal_congregation() {
        return local_congregation;
    }

    public void setLocal_congregation(String local_congregation) {
        this.local_congregation = local_congregation;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    @Override
    public String toString() {
        return
//                image + "\n" +
//                "username='" + username + "\n" +
//                "phone='" + phone ;

                firstname + "\n" +
                        "surname='" + surname + "\n" +
                        "othername='" + othername + "\n" +
                        "school'" + school + "\n" +
                        "level='" + year + "\n" +
                        "parttype='" + participant_type + "\n" +
                        "phone='" + phone;

    }
}
